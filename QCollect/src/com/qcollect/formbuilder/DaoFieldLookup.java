package com.qcollect.formbuilder;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import com.qcollect.main.ConnectionClass;

public class DaoFieldLookup {

	public static SessionFactory factory = ConnectionClass.getFactory();
	static Logger log = Logger.getLogger(DaoFieldLookup.class.getName());
	
	
	public ArrayList<BeanFieldLookup> getFieldLookups()
	{
		// Declarations
		ArrayList<BeanFieldLookup> lookupList=new ArrayList<BeanFieldLookup>();  
		// Open session from session factory
		Session session = factory.openSession();
		try {
				Criteria cr=session.createCriteria(BeanFieldLookup.class);
				lookupList=new ArrayList<BeanFieldLookup>(cr.list());
				return lookupList;

		} catch (Exception e) {
			e.printStackTrace();
			return lookupList;

		} finally {
			// close session
			session.close();
		}
	}
	
	public ArrayList<BeanFieldLookup> getFieldLookups(ArrayList<Integer>lookupIds)
	{
		// Declarations
		ArrayList<BeanFieldLookup> lookupList=new ArrayList<BeanFieldLookup>();  
		// Open session from session factory
		Session session = factory.openSession();
		try {
				Criteria cr=session.createCriteria(BeanFieldLookup.class);
				cr.add(Restrictions.in("lookup_id", lookupIds));
				lookupList=new ArrayList<BeanFieldLookup>(cr.list());
				return lookupList;

		} catch (Exception e) {
			e.printStackTrace();
			return lookupList;

		} finally {
			// close session
			session.close();
		}
	}
}
