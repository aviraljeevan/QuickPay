package com.qcollect.formbuilder;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "lookup_form_fields")
public class BeanFieldLookup {

	
	@GenericGenerator(name = "g1", strategy = "increment")
	@Id
	@GeneratedValue(generator = "g1")
	private Integer lookup_id;
	private String lookup_name,lookup_type,lookup_subtype;
	private Integer isPredefined;
	public Integer getLookup_id() {
		return lookup_id;
	}
	public void setLookup_id(Integer lookup_id) {
		this.lookup_id = lookup_id;
	}
	public String getLookup_name() {
		return lookup_name;
	}
	public void setLookup_name(String lookup_name) {
		this.lookup_name = lookup_name;
	}
	public String getLookup_type() {
		return lookup_type;
	}
	public void setLookup_type(String lookup_type) {
		this.lookup_type = lookup_type;
	}
	public Integer getIsPredefined() {
		return isPredefined;
	}
	public void setIsPredefined(Integer isPredefined) {
		this.isPredefined = isPredefined;
	}
	public String getLookup_subtype() {
		return lookup_subtype;
	}
	public void setLookup_subtype(String lookup_subtype) {
		this.lookup_subtype = lookup_subtype;
	}
	
	
	
	
}
