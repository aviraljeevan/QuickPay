package com.qcollect.formbuilder;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;
import com.qcollect.demo.SampleFormView;
import com.qcollect.fees.BeanFeeLookup;
import com.qcollect.fees.DaoFee;

public class ActionsFormBuilder extends ActionSupport{
	
	HttpServletRequest request = ServletActionContext.getRequest();
	HttpSession ses = request.getSession();
	static Logger log = Logger.getLogger(ActionsFormBuilder.class.getName());
	private DaoFieldLookup daoLookup=new DaoFieldLookup();
	private DaoFee daofee=new DaoFee();
	private ArrayList<BeanFieldLookup>fields=new ArrayList<BeanFieldLookup>();
	private ArrayList<BeanFeeLookup>fees=new ArrayList<BeanFeeLookup>();
	private ArrayList<String>selectedFields=new ArrayList<String>();
	private ArrayList<String>selectedFees=new ArrayList<String>();
	private ArrayList<BeanFieldLookup>renderLookups=new ArrayList<BeanFieldLookup>();
	private Map<Integer, List<String>>optionsMap=new HashMap<Integer, List<String>>();
	public String initBuilder()
	{
		ArrayList<BeanFieldLookup>fieldLookupList=new ArrayList<BeanFieldLookup>();
		ArrayList<BeanFeeLookup>feeLookupList=new ArrayList<BeanFeeLookup>();
		ArrayList<Integer>fieldIds=new ArrayList<Integer>();
		HashMap<Integer,String>renderMap=new HashMap<Integer, String>();
		HashMap<Integer,ArrayList<String>>valuesMap=new HashMap<Integer,ArrayList<String>>();
		HashMap<Integer, List<String>>optionsMap=new HashMap<Integer, List<String>>();
		try
		{
			fieldLookupList=daoLookup.getFieldLookups();
			feeLookupList=daofee.getFeeLookups();
			
			fields=fieldLookupList;
			fees=feeLookupList;
			
			ses.setAttribute("sesRenderMap", renderMap);
			ses.setAttribute("sesValuesMap", valuesMap);
			ses.setAttribute("sesFieldsList", fieldIds);
			ses.setAttribute("sesOptionsMap", optionsMap);
			return SUCCESS;
		}
		finally
		{
			
		}
	}
	
	public String saveOptions()
	{
		String optionArray;
		Integer fieldId;
		HashMap<Integer,List<String>>optionMap=new HashMap<Integer,List<String>>();
		try
		{
			optionArray=request.getParameter("optionArray");
			fieldId=Integer.parseInt(request.getParameter("fieldId").trim());
			optionMap=(HashMap<Integer, List<String>>) ses.getAttribute("sesOptionsMap");
			List<String>optionList=Arrays.asList(optionArray.split(","));
			optionMap.put(fieldId, optionList);
			ses.setAttribute("sesOptionsMap", optionMap);
			return SUCCESS;
		}
		finally
		{
			
		}
	}
	public String renderPreview()
	{
		String selectedFields;
		Integer baseAmount;
		HashMap<Integer,String>renderMap=new HashMap<Integer, String>();
		HashMap<Integer,ArrayList<String>>valuesMap=new HashMap<Integer,ArrayList<String>>();
		HashMap<Integer,List<String>>optionMap=new HashMap<Integer,List<String>>();
		ArrayList<Integer>renderKeySet;
		ArrayList<Integer>valuesKeySet;
		ArrayList<Integer>fieldIds;
		
		try
		{
			fieldIds=(ArrayList<Integer>) ses.getAttribute("sesFieldsList");
			optionMap=(HashMap<Integer, List<String>>) ses.getAttribute("sesOptionsMap");
			selectedFields=request.getParameter("fieldarray");
			List<String>fieldList=Arrays.asList(selectedFields.split(","));
			log.info("selected Fields are "+fieldList.toString());
			Iterator<String>fieldIdIterator=fieldList.iterator();
			while(fieldIdIterator.hasNext())//Loop to add new Fields
			{
				boolean add=true;
				Integer selectedId;
				try {
					selectedId = Integer.parseInt(fieldIdIterator.next());
				} catch (NumberFormatException e) {
					// TODO Auto-generated catch block
					log.info("breaking from add to List while loop");
					break;
				}
				Iterator<Integer>currentIdIterator=fieldIds.iterator();
				while(currentIdIterator.hasNext())
				{
					Integer currentId=currentIdIterator.next();
					if(currentId==selectedId)
					{
						add=false;
					}
				}
				if(add)
				{
					fieldIds.add(selectedId);
				}
			}
			Iterator<Integer>currentIt=fieldIds.iterator();
			
			while(currentIt.hasNext())//Loop to remove fields
			{
				boolean remove=true;
				Integer currId=currentIt.next();
				Iterator<String>selectedIt=fieldList.iterator();
				while(selectedIt.hasNext())
				{
					Integer selId;
					try {
						selId = Integer.parseInt(selectedIt.next());
						if(selId==currId)
						{
							remove=false;
						}
					} catch (NumberFormatException e) {
						// TODO Auto-generated catch block
						remove=true;
					}
					
				}
				if(remove)
				{
					currentIt.remove();
				}
			}
			baseAmount=Integer.parseInt(request.getParameter("feeAmount"));
			log.info("SelectedFields are "+fieldIds.toString()+" Amount is "+baseAmount);
			
			ArrayList<BeanFieldLookup>currentFieldLookups=new ArrayList<BeanFieldLookup>();
			if(!fieldIds.isEmpty())
			{
				currentFieldLookups=daoLookup.getFieldLookups(fieldIds);
			}
			log.info(currentFieldLookups);
			HashMap<Integer,ArrayList<String>>optMap=(HashMap<Integer, ArrayList<String>>) ses.getAttribute("sesOptionsMap");
			//Look For Fields with missing options
			if(!currentFieldLookups.isEmpty())
			{
				ArrayList<Integer>optionKeys=new ArrayList<Integer>(optMap.keySet());
				Iterator<BeanFieldLookup>it=currentFieldLookups.iterator();
				while(it.hasNext())
				{
					BeanFieldLookup temp=it.next();
					String temptype=temp.getLookup_type();
					Integer tempid=temp.getLookup_id();
					if(temptype.contentEquals("Select Box"))
					{
						
						if(optionKeys.isEmpty())
						{
							request.setAttribute("optionName", temp.getLookup_name());
							request.setAttribute("optionId", temp.getLookup_id());
							return "options";
						}
						else
						{
							boolean optionpage=true;
							for (int i = 0; i < optionKeys.size(); i++) {
								Integer key=optionKeys.get(i);
								if(key==tempid)
								{
									optionpage=false;
								}
								
							}
							if(optionpage){
								request.setAttribute("optionName", temp.getLookup_name());
								request.setAttribute("optionId", temp.getLookup_id());
								return "options";
							}
						}
					}
					
					
				}
			}
			renderLookups=currentFieldLookups;
			renderMap=(HashMap<Integer, String>) ses.getAttribute("sesRenderMap");
			valuesMap=(HashMap<Integer, ArrayList<String>>) ses.getAttribute("sesValuesMap");
			renderKeySet=new ArrayList<Integer>(renderMap.keySet());
		
			if(renderKeySet.isEmpty())
			{
				
			}
			optionsMap=(Map<Integer, List<String>>) ses.getAttribute("sesOptionsMap");
			return SUCCESS;
		}
		finally
		{
			
		}
		
		
	}
	
	private ArrayList<SampleFormView> processFormView(HashMap<Integer,String>formDataMap )
	{
		ArrayList<SampleFormView> formViewList=new ArrayList<SampleFormView>();
		SampleFormView formViewData;
		try
		{
			ArrayList<Integer>keySet=new ArrayList<Integer>(formDataMap.keySet());
			for (int i = 0; i < keySet.size(); i++) {
				String[] formFieldValue=formDataMap.get(keySet.get(i)).split("=");
				if(formFieldValue.length!=2)
				{
					log.info("Form Field Value corrupt or Unreadable");
				}
				else
				{
					formViewData=new SampleFormView();
					formViewData.setLabel(formFieldValue[0]);
					formViewData.setValue(formFieldValue[1]);
					formViewList.add(formViewData);
				}
			}
			return formViewList;
			
		}
		finally
		{
			
		}
	}

	public ArrayList<BeanFieldLookup> getFields() {
		return fields;
	}

	public void setFields(ArrayList<BeanFieldLookup> fields) {
		this.fields = fields;
	}

	public ArrayList<BeanFeeLookup> getFees() {
		return fees;
	}

	public void setFees(ArrayList<BeanFeeLookup> fees) {
		this.fees = fees;
	}

	public ArrayList<String> getSelectedFields() {
		return selectedFields;
	}

	public void setSelectedFields(ArrayList<String> selectedFields) {
		this.selectedFields = selectedFields;
	}

	public ArrayList<String> getSelectedFees() {
		return selectedFees;
	}

	public void setSelectedFees(ArrayList<String> selectedFees) {
		this.selectedFees = selectedFees;
	}

	public ArrayList<BeanFieldLookup> getRenderLookups() {
		return renderLookups;
	}

	public void setRenderLookups(ArrayList<BeanFieldLookup> renderLookups) {
		this.renderLookups = renderLookups;
	}

	public Map<Integer, List<String>> getOptionsMap() {
		return optionsMap;
	}

	public void setOptionsMap(Map<Integer, List<String>> optionsMap) {
		this.optionsMap = optionsMap;
	}

	

	
	

	
	
	
	
	
	
}
