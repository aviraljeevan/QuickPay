package com.qcollect.formbuilder;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.qcollect.fees.BeanFeeDetails;

@Entity
@Table(name = "data_form_details")
public class BeanFormDetails {

	@GenericGenerator(name = "g1", strategy = "increment")
	@Id
	@GeneratedValue(generator = "g1")
	private Integer id;
	private String formName;
	private Date formDate;
	private Integer formOwnerId;
	
	@OneToOne(cascade = CascadeType.ALL,orphanRemoval=true)
	private BeanFormStructure formStructure;
	
	@OneToOne(cascade = CascadeType.ALL,orphanRemoval=true)
	private BeanFeeDetails formFee;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFormName() {
		return formName;
	}

	public void setFormName(String formName) {
		this.formName = formName;
	}

	public Date getFormDate() {
		return formDate;
	}

	public void setFormDate(Date formDate) {
		this.formDate = formDate;
	}

	public Integer getFormOwnerId() {
		return formOwnerId;
	}

	public void setFormOwnerId(Integer formOwnerId) {
		this.formOwnerId = formOwnerId;
	}

	public BeanFormStructure getFormStructure() {
		return formStructure;
	}

	public void setFormStructure(BeanFormStructure formStructure) {
		this.formStructure = formStructure;
	}

	public BeanFeeDetails getFormFee() {
		return formFee;
	}

	public void setFormFee(BeanFeeDetails formFee) {
		this.formFee = formFee;
	}
	
	
	
}
