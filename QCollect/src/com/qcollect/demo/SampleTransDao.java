package com.qcollect.demo;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;

import com.qcollect.main.ConnectionClass;

public class SampleTransDao {

	public static SessionFactory factory = ConnectionClass.getFactory();
	static Logger log = Logger.getLogger(SampleTransDao.class.getName());

	public SampleTransBean saveTransaction(SampleTransBean beanTransData) {
		// Declarations

		// Open session from session factory
		Session session = factory.openSession();
		try {
			session.beginTransaction();
			session.saveOrUpdate(beanTransData);
			session.getTransaction().commit();
			return beanTransData;

		} catch (Exception e) {
			e.printStackTrace();
			return beanTransData;
		} finally {
			// close session
			session.close();
		}
	}

	public ArrayList<SampleTransBean> getTransactions() {
		// Declarations
		ArrayList<SampleTransBean> resultList = new ArrayList<SampleTransBean>();
		// Open session from session factory
		Session session = factory.openSession();
		try {

			Criteria cr = session.createCriteria(SampleTransBean.class);
			resultList = new ArrayList<SampleTransBean>(cr.list());
			return resultList;

		} catch (Exception e) {
			e.printStackTrace();
			return resultList;
		} finally {
			// close session
			session.close();
		}
	}

	public SampleTransBean getTransactions(Integer transId) {
		// Declarations
		SampleTransBean resultList = new SampleTransBean();
		// Open session from session factory
		Session session = factory.openSession();
		try {

			Criteria cr = session.createCriteria(SampleTransBean.class);
			cr.add(Restrictions.eq("id", transId));
			resultList = (SampleTransBean) cr.list().get(0);
			return resultList;

		} catch (Exception e) {
			e.printStackTrace();
			return resultList;
		} finally {
			// close session
			session.close();
		}
	}

	public List<SampleTransBean> payerTransactionHistoryDao(String enrollmentNumber) {
		Session session = factory.openSession();
		Criteria criteria = session.createCriteria(SampleTransBean.class);
		criteria.add(Restrictions.eq("enrollmentNumber", enrollmentNumber));
		List<SampleTransBean> sampleFormBeans = criteria.list();
		return sampleFormBeans;
	}

	public SampleTransBean getTransactionsDetails(String transId) {
		SampleTransBean resultList = new SampleTransBean();
		// Open session from session factory
		Session session = factory.openSession();
		try {

			Criteria cr = session.createCriteria(SampleTransBean.class);
			cr.add(Restrictions.eq("transId", transId));
			resultList = (SampleTransBean) cr.list().get(0);
			return resultList;

		} catch (Exception e) {
			e.printStackTrace();
			return resultList;
		} finally {
			// close session
			session.close();
		}
	}

	@SuppressWarnings("unchecked")
	public ArrayList<SampleTransBean> getTransactionsDetails(String feeName, String fromDate, String toDate,
			String payMode) throws ParseException {

		/*
		 * String val =parameter2; DateFormat sdf = new
		 * SimpleDateFormat("yyyy-MM-dd"); Date startDate = sdf.parse(val);
		 */
		/* mm/dd/yyy */
		String date1 = fromDate;
		Date dateFrom1=null;
		Date endDate=null;
		try {
			SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
			dateFrom1 = sdf.parse(date1);
			String val1 = toDate;
			DateFormat sdf1 = new SimpleDateFormat("yyyy-MM-dd");
			endDate = sdf1.parse(val1);
		} catch (java.text.ParseException n) {
			dateFrom1=null;
			endDate=null;
		}

		Session session = factory.openSession();

		Criteria criteria = session.createCriteria(SampleTransBean.class);
		if (feeName != null && !feeName.contentEquals("")) {
			criteria.add(Restrictions.eq("feeNames", feeName));
		}
		if (payMode != null && !payMode.contentEquals("")) {
			criteria.add(Restrictions.eq("transPaymode", payMode));
		}
		if (fromDate != null && !fromDate.contentEquals("")) {
			criteria.add(Restrictions.between("transDate", dateFrom1, endDate));
		}
		ArrayList<SampleTransBean> formBean = (ArrayList<SampleTransBean>) criteria.list();

		return formBean;
	}

}
