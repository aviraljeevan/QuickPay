package com.qcollect.demo;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;
import org.hibernate.annotations.Type;

@Entity
@Table(name = "data_form")
public class SampleFormBean {

	@GenericGenerator(name = "g1", strategy = "increment")
	@Id
	@GeneratedValue(generator = "g1")
	private Integer formId;
	@Type(type="text")
	private String formData;
	private Integer formTemplateId;
	private String formTransId;
	private Date formDate;
	private Integer formInstId, formApplicantId, formFeeId;
    @OneToOne
	private SampleTransBean transBean ;
	
	
	public SampleTransBean getTransBean() {
		return transBean;
	}

	public void setTransBean(SampleTransBean transBean) {
		this.transBean = transBean;
	}

	public Integer getFormId() {
		return formId;
	}

	public void setFormId(Integer formId) {
		this.formId = formId;
	}

	public String getFormData() {
		return formData;
	}

	public void setFormData(String formData) {
		this.formData = formData;
	}

	public Integer getFormTemplateId() {
		return formTemplateId;
	}

	public void setFormTemplateId(Integer formTemplateId) {
		this.formTemplateId = formTemplateId;
	}

	

	public String getFormTransId() {
		return formTransId;
	}

	public void setFormTransId(String formTransId) {
		this.formTransId = formTransId;
	}

	public Date getFormDate() {
		return formDate;
	}

	public void setFormDate(Date formDate) {
		this.formDate = formDate;
	}

	public Integer getFormInstId() {
		return formInstId;
	}

	public void setFormInstId(Integer formInstId) {
		this.formInstId = formInstId;
	}

	public Integer getFormApplicantId() {
		return formApplicantId;
	}

	public void setFormApplicantId(Integer formApplicantId) {
		this.formApplicantId = formApplicantId;
	}

	public Integer getFormFeeId() {
		return formFeeId;
	}

	public void setFormFeeId(Integer formFeeId) {
		this.formFeeId = formFeeId;
	}
	

	public SampleFormBean(String formData, Integer formTemplateId,  Date formDate,
			Integer formInstId, Integer formApplicantId, Integer formFeeId) {
		super();
		this.formData = formData;
		this.formTemplateId = formTemplateId;
		this.formDate = formDate;
		this.formInstId = formInstId;
		this.formApplicantId = formApplicantId;
		this.formFeeId = formFeeId;
	}

	public SampleFormBean() {
		// TODO Auto-generated constructor stub
	}

	
	
	

}
