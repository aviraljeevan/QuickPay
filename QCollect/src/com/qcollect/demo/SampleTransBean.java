package com.qcollect.demo;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.dexpert.qcollect.college.CollegeBean;

@Entity
@Table(name = "data_transactions")
public class SampleTransBean {

	@GenericGenerator(name = "g1", strategy = "increment")
	@Id
	@GeneratedValue(generator = "g1")
	private Integer id;
	private String transId,transStatus,transPaymode,transIdExt,feeNames;
	private Date transDate;
	private Double transAmount,transCharges;
	private String referenceNo,name;
	private String bankReferenceNo;
	private String category;
	private String enrollmentNumber; 
	//bidirectional relation with transaction bean
	@ManyToOne(targetEntity=CollegeBean.class)
	@JoinColumn(name="college_Id_fk",referencedColumnName="collegeId")
	private CollegeBean collegeBean;
	
	public CollegeBean getCollegeBean() {
		return collegeBean;
	}
	public void setCollegeBean(CollegeBean collegeBean) {
		this.collegeBean = collegeBean;
	}
	public String getReferenceNo() {
		return referenceNo;
	}
	public void setReferenceNo(String referenceNo) {
		this.referenceNo = referenceNo;
	}
	public String getBankReferenceNo() {
		return bankReferenceNo;
	}
	public void setBankReferenceNo(String bankReferenceNo) {
		this.bankReferenceNo = bankReferenceNo;
	}
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	@OneToOne(cascade = CascadeType.ALL,orphanRemoval=true)
	private SampleFormBean transForm;
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getTransId() {
		return transId;
	}
	public void setTransId(String transId) {
		this.transId = transId;
	}
	public String getTransStatus() {
		return transStatus;
	}
	public void setTransStatus(String transStatus) {
		this.transStatus = transStatus;
	}
	public String getTransPaymode() {
		return transPaymode;
	}
	public void setTransPaymode(String transPaymode) {
		this.transPaymode = transPaymode;
	}
	public Date getTransDate() {
		return transDate;
	}
	public void setTransDate(Date transDate) {
		this.transDate = transDate;
	}
	public SampleFormBean getTransForm() {
		return transForm;
	}
	public void setTransForm(SampleFormBean transForm) {
		this.transForm = transForm;
	}
	public String getTransIdExt() {
		return transIdExt;
	}
	public void setTransIdExt(String transIdExt) {
		this.transIdExt = transIdExt;
	}
	public Double getTransAmount() {
		return transAmount;
	}
	public void setTransAmount(Double transAmount) {
		this.transAmount = transAmount;
	}
	public Double getTransCharges() {
		return transCharges;
	}
	public void setTransCharges(Double transCharges) {
		this.transCharges = transCharges;
	}
	public String getEnrollmentNumber() {
		return enrollmentNumber;
	}
	public void setEnrollmentNumber(String enrollmentNumber) {
		this.enrollmentNumber = enrollmentNumber;
	}
	public String getFeeNames() {
		return feeNames;
	}
	public void setFeeNames(String feeNames) {
		this.feeNames = feeNames;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	
	
	
}
