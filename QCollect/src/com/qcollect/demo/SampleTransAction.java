package com.qcollect.demo;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;

public class SampleTransAction extends ActionSupport {

	HttpServletRequest request = ServletActionContext.getRequest();
	HttpSession ses = request.getSession();
	static Logger log = Logger.getLogger(SampleTransAction.class.getName());
	private SampleTransDao daoTrans = new SampleTransDao();
	private SampleTransBean beanTransData;
	ArrayList<SampleTransBean> transData = new ArrayList<SampleTransBean>();
	List<SampleTransBean> payerHistory = new ArrayList<SampleTransBean>();
	ArrayList<SampleFormView> simpleFormViewslist=new ArrayList<SampleFormView>();

	private SampleFormBean sampleFormBean = new SampleFormBean();

	public ArrayList<SampleFormView> getSimpleFormViewslist() {
		return simpleFormViewslist;
	}

	public void setSimpleFormViewslist(ArrayList<SampleFormView> simpleFormViewslist) {
		this.simpleFormViewslist = simpleFormViewslist;
	}

	@SuppressWarnings("unchecked")
	public String prepareTransaction() {
		SampleFormBean beanCurrentForm;
		SampleTransBean beanCurrentTrans;
		Date transDate = new Date();

		String transId;
		try {
			simpleFormViewslist=(ArrayList<SampleFormView>)ses.getAttribute("formViewData");
			
			Map<String,String> dataMapDetails=new HashMap<String,String>();
			for (SampleFormView sBean : simpleFormViewslist) {
				int i=1;
				log.info("lable="+sBean.getLabel()+" :"+i+" value="+sBean.getValue());
				dataMapDetails.put(sBean.getLabel(), sBean.getValue());
				i++;
			}
			
			ses.setAttribute("dataMapDetails",dataMapDetails);
			log.info(dataMapDetails.size()+":the of the List is ");
			log.info("Total Amount= "+dataMapDetails.get("Total Amount"));
			Double transAmount = Double.parseDouble(dataMapDetails.get("Total Amount"));
			beanCurrentForm = (SampleFormBean) ses.getAttribute("sesCurrentFormData");
			beanCurrentTrans = new SampleTransBean();
			beanCurrentTrans.setEnrollmentNumber(dataMapDetails.get("Roll Number"));
			beanCurrentTrans.setFeeNames(dataMapDetails.get("Payment Category"));
			beanCurrentTrans.setName(dataMapDetails.get("firstName")+" "+dataMapDetails.get("lstName"));
			beanCurrentTrans.setTransDate(transDate);
			
			beanCurrentTrans.setTransStatus("Pending");

			beanCurrentTrans = daoTrans.saveTransaction(beanCurrentTrans);

			transId = GenerateTransactionId(beanCurrentTrans.getId());

			beanCurrentTrans.setTransId(transId);
			beanCurrentTrans.setTransAmount(transAmount);

			beanCurrentForm.setFormTransId(transId);

			beanCurrentTrans.setTransForm(beanCurrentForm);
			beanCurrentTrans.setTransPaymode(beanCurrentTrans.getTransPaymode());

			beanCurrentTrans = daoTrans.saveTransaction(beanCurrentTrans);

			ses.setAttribute("sesCurrentTransData", beanCurrentTrans);

			beanTransData = new SampleTransBean();
			beanTransData = beanCurrentTrans;
			return SUCCESS;

		} finally {

		}
	}

	public String completeTransaction() {
		String status;
		SampleTransBean beanCurrentTrans;
		try {
			
			/*payMode,status,amount,issuerRefNo,SabPaisaTxId*/
			
			
			
			beanCurrentTrans = new SampleTransBean();
			beanCurrentTrans = (SampleTransBean) ses.getAttribute("sesCurrentTransData");
			status = request.getParameter("status");
			if(request.getParameter("payMode")!="Cash"){
				
				if(request.getParameter("issuerRefNo")!=null){
				beanCurrentTrans.setBankReferenceNo(request.getParameter("issuerRefNo"));
				beanCurrentTrans.setReferenceNo(request.getParameter("SabPaisaTxId"));
			}
			
				
			}
			//log.info("status:"+request.getParameter("payMode")+" "+request.getParameter("issuerRefNo")+" "+request.getParameter("SabPaisaTxId")+" "+request.getParameter("status"));
			
		
			/*String bkReferenceNo = Math.random() + "";*/
		/*	bkReferenceNo = bkReferenceNo.substring(3, 8);*/
			/*String bankReferenceNo1 = "UNBK" + request.getParameter("issuerRefNo");
			bankReferenceNo1 = bankReferenceNo1.substring(0, 8);*/
			
			

			beanCurrentTrans.setTransPaymode(request.getParameter("payMode"));
			
			/*
			 * beanCurrentTrans.getCollegeBean().setCollegeName(
			 * "Council of Higher Secondary Education, Odisha");
			 * beanCurrentTrans.getCollegeBean().setCollegeCode("20459");
			 */
			//beanCurrentTrans.setTransCharges(Double.parseDouble(request.getParameter("transCharges")));

			if (status.contentEquals("Ok")) {
				beanCurrentTrans.setTransStatus("Successful");

			} else {
				beanCurrentTrans.setTransStatus("Failed");
			}
			beanCurrentTrans = daoTrans.saveTransaction(beanCurrentTrans);
			beanTransData = beanCurrentTrans;
			return SUCCESS;
		} finally {

		}
	}

	public String getAllTransactions() {
		String forDownload = request.getParameter("ForDownLoad");
		ArrayList<SampleTransBean> transList;
		try {
			transList = new ArrayList<SampleTransBean>();
			transList = daoTrans.getTransactions();
			transData = transList;
			String result = forDownload != null && forDownload.contentEquals("True") ? "forDownload" : "success";
			log.info("result is" + result);
			return result;
		} finally {

		}
	}

	private String GenerateTransactionId(Integer transInternalId) {
		String transId = "QC";
		int i = 10 - transInternalId.toString().length();

		for (int j = 0; j < i; j++) {

			transId = transId.concat("0");
		}
		transId = transId.concat(transInternalId.toString());
		return transId;

	}

	// method for getting transaction of payer
	public String payerTransactionHistory() {
		String enrollmentNumber = request.getParameter("enrollmentNumber");
		payerHistory = daoTrans.payerTransactionHistoryDao(enrollmentNumber);
		log.info("list size is" + payerHistory.size());

		return SUCCESS;
	}

	public String viewReceiptDetailsAction() {

		beanTransData = new SampleTransBean();
		log.info(request.getParameter("tid"));
		beanTransData = daoTrans.getTransactionsDetails(request.getParameter("tid"));

		/* sampleFormBean=SampleFormDao.getFormDetails() */
		beanTransData.getCollegeBean();

		log.info(beanTransData.getBankReferenceNo() + "bank ref");

		return SUCCESS;
	}

	public String searchBasedOnCriteriaAction() throws ParseException {

		/*
		 * var
		 * query="?payCart="+payCart+"&frmDate="+frmDate+"&tDate="+tDate+"&payMode="
		 * +payMode;
		 */
		log.info(request.getParameter("payCart") + " " + request.getParameter("frmDate") + " "
				+ request.getParameter("tDate") + " " + request.getParameter("payMode"));

		transData = daoTrans.getTransactionsDetails(
				request.getParameter("payCart").contentEquals("Select An Option") ? null : request
						.getParameter("payCart"),
				request.getParameter("frmDate"),
				request.getParameter("tDate"),
				request.getParameter("payMode").contentEquals("Select An Option") ? null : request
						.getParameter("payMode"));
		String feeName=request.getParameter("payCart").contentEquals("Select An Option") ? null : request
				.getParameter("payCart");
		log.info("Fee Name"+feeName);
		log.info(transData.size());
		return SUCCESS;
	}

	public SampleTransBean getBeanTransData() {
		return beanTransData;
	}

	public void setBeanTransData(SampleTransBean beanTransData) {
		this.beanTransData = beanTransData;
	}

	public ArrayList<SampleTransBean> getTransData() {
		return transData;
	}

	public void setTransData(ArrayList<SampleTransBean> transData) {
		this.transData = transData;
	}

	public List<SampleTransBean> getPayerHistory() {
		return payerHistory;
	}

	public void setPayerHistory(List<SampleTransBean> payerHistory) {
		this.payerHistory = payerHistory;
	}

	public SampleFormBean getSampleFormBean() {
		return sampleFormBean;
	}

	public void setSampleFormBean(SampleFormBean sampleFormBean) {
		this.sampleFormBean = sampleFormBean;
	}

}
