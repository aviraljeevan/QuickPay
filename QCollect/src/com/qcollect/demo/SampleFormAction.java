package com.qcollect.demo;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;

public class SampleFormAction extends ActionSupport{

	HttpServletRequest request = ServletActionContext.getRequest();
	HttpSession ses = request.getSession();
	static Logger log = Logger.getLogger(SampleFormAction.class.getName());
	private SampleFormDao daoForm=new SampleFormDao() ;
	private SampleTransDao daoTrans = new SampleTransDao();
	ArrayList<SampleFormView> formViewData=new ArrayList<SampleFormView>();
	
	public String prepareForm()//Process Form Data, save to session and show form and amount summary before continuing to pay
	{
		
		log.info("hello testing the coding.................");
		String formData;
		Integer formTemplateId,formInstId,formFeeId,formApplicantId;
		Date formSubmitDate=new Date();
		SampleFormBean beanFormData;
		HashMap<Integer,String>formDataMap;
		try
		{
			formData=request.getParameter("values");
			formData=formData.concat(",-1`Date="+formSubmitDate.toString());
			formTemplateId=0;
			log.info(request.getParameter("instId"));
			log.info(request.getParameter("feeId"));
			/*formInstId=Integer.parseInt(request.getParameter("instId"));
			formFeeId=Integer.parseInt(request.getParameter("feeId"));*/
			formInstId=23;
			formFeeId=26;
			formApplicantId=0;
			
			//Populate Form Bean to persist later if transaction is done (Either Successful or Fail)
			beanFormData=new SampleFormBean(formData, formTemplateId, formSubmitDate, formInstId, formApplicantId, formFeeId);
			ses.setAttribute("sesCurrentFormData", beanFormData);
			
			//Process Form Data to Show Summary
			formDataMap=new HashMap<Integer, String>();
			
			
	
			formDataMap=processFormData(formData);
			
			ses.setAttribute("sesCurrentFormMap", formDataMap);
			
			formViewData=processFormView(formDataMap);
			
			ses.setAttribute("formViewData", formViewData);
			
			return SUCCESS;
			
		}
		finally{
			
		}
		
		
	}
	
	private HashMap<Integer,String> processFormData(String formDataRaw)
	{
		HashMap<Integer,String>formDataMap;
		try
		{
			formDataMap=new HashMap<Integer, String>();
			
			String[]formFields=formDataRaw.split(",");
			ArrayList<String> formFieldsList = new ArrayList<String>(Arrays.asList(formFields));
			
			log.info("formFieldsList is "+formFieldsList.toString());
			
			for (int i = 0; i < formFieldsList.size(); i++) {
				String[]formFieldData=formFieldsList.get(i).split("`");
				if(formFieldData.length!=2)
				{
					log.info("Form Field at index "+i+" is corrupt or unreadable");
				}
				else
				{
					formDataMap.put(Integer.parseInt(formFieldData[0].trim()), formFieldData[1]);
				}
			}
			return formDataMap;
			
		}
		finally
		{
			
		}
	}
	public String viewForm()
	{
		Integer transID;
		SampleTransBean beanTransData;
		try
		{
			beanTransData=new SampleTransBean();
			transID=Integer.parseInt(request.getParameter("transId"));
			beanTransData=daoTrans.getTransactions(transID);
			HashMap<Integer,String>formDataMap=new HashMap<Integer, String>();
			
			
			formDataMap=processFormData(beanTransData.getTransForm().getFormData());
			
			ses.setAttribute("sesCurrentFormMap", formDataMap);
			
			formViewData=processFormView(formDataMap);
			return SUCCESS;
		}
		finally
		{
			
		}
	}
	
	private ArrayList<SampleFormView> processFormView(HashMap<Integer,String>formDataMap )
	{
		ArrayList<SampleFormView> formViewList=new ArrayList<SampleFormView>();
		SampleFormView formViewData;
		try
		{
			ArrayList<Integer>keySet=new ArrayList<Integer>(formDataMap.keySet());
			for (int i = 0; i < keySet.size(); i++) {
				String[] formFieldValue=formDataMap.get(keySet.get(i)).split("=");
				if(formFieldValue.length!=2)
				{
					log.info("Form Field Value corrupt or Unreadable");
				}
				else
				{
					formViewData=new SampleFormView();
					formViewData.setLabel(formFieldValue[0]);
					formViewData.setValue(formFieldValue[1]);
					formViewList.add(formViewData);
				}
			}
			return formViewList;
			
		}
		finally
		{
			
		}
	}

	public ArrayList<SampleFormView> getFormViewData() {
		return formViewData;
	}

	public void setFormViewData(ArrayList<SampleFormView> formViewData) {
		this.formViewData = formViewData;
	}

	public SampleFormDao getDaoForm() {
		return daoForm;
	}

	public void setDaoForm(SampleFormDao daoForm) {
		this.daoForm = daoForm;
	}

	
	
	
	
	
	

	
	
}
