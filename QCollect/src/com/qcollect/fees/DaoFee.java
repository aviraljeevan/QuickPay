package com.qcollect.fees;

import java.util.ArrayList;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.qcollect.formbuilder.DaoFieldLookup;
import com.qcollect.main.ConnectionClass;

public class DaoFee {

	
	public static SessionFactory factory = ConnectionClass.getFactory();
	static Logger log = Logger.getLogger(DaoFieldLookup.class.getName());
	
	public ArrayList<BeanFeeLookup> getFeeLookups()
	{
		// Declarations
		ArrayList<BeanFeeLookup>feeLookupList=new ArrayList<BeanFeeLookup>();
		Session session = factory.openSession();
		try {
			// Open session from session factory
			
			Criteria cr=session.createCriteria(BeanFeeLookup.class);
			feeLookupList=new ArrayList<BeanFeeLookup>(cr.list());
			
			return feeLookupList;

		} catch (Exception e) {
			e.printStackTrace();
			return feeLookupList;

		} finally {
			// close session
			session.close();
		}
	}
	
	public BeanFeeLookup saveFeeLookup(BeanFeeLookup lookupData)
	{
		// Declarations

		// Open session from session factory
		Session session = factory.openSession();
		try {
			session.beginTransaction();
			session.saveOrUpdate(lookupData);
			session.getTransaction().commit();
			return lookupData;

		} catch (Exception e) {
			e.printStackTrace();
			return lookupData;

		} finally {
			// close session
			session.close();
		}
	}
}
