package com.qcollect.fees;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

@Entity
@Table(name = "lookup_fee")
public class BeanFeeLookup {

	@GenericGenerator(name = "g1", strategy = "increment")
	@Id
	@GeneratedValue(generator = "g1")
	private Integer id;
	private String feeName;
	private Integer feeOwnerId;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getFeeName() {
		return feeName;
	}
	public void setFeeName(String feeName) {
		this.feeName = feeName;
	}
	public Integer getFeeOwnerId() {
		return feeOwnerId;
	}
	public void setFeeOwnerId(Integer feeOwnerId) {
		this.feeOwnerId = feeOwnerId;
	}
	
	
}
