package com.qcollect.fees;

import java.util.Date;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.qcollect.fees.latefee.BeanLateFee;

@Entity
@Table(name = "data_fee_details")
public class BeanFeeDetails {

	@GenericGenerator(name = "g1", strategy = "increment")
	@Id
	@GeneratedValue(generator = "g1")
	private Integer id;
	private Date feeStartDate,feeEndDate,feeLateEndDate;
	private Double feeBaseAmount;
	private Integer isLatefeeApplicable;
	
	@OneToOne(cascade = CascadeType.ALL,orphanRemoval=true)
	private BeanLateFee latefee;
	
	@OneToOne(cascade = CascadeType.ALL,orphanRemoval=true)
	private BeanFeeLookup feeLookup;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Date getFeeStartDate() {
		return feeStartDate;
	}

	public void setFeeStartDate(Date feeStartDate) {
		this.feeStartDate = feeStartDate;
	}

	public Date getFeeEndDate() {
		return feeEndDate;
	}

	public void setFeeEndDate(Date feeEndDate) {
		this.feeEndDate = feeEndDate;
	}

	public Date getFeeLateEndDate() {
		return feeLateEndDate;
	}

	public void setFeeLateEndDate(Date feeLateEndDate) {
		this.feeLateEndDate = feeLateEndDate;
	}

	public Double getFeeBaseAmount() {
		return feeBaseAmount;
	}

	public void setFeeBaseAmount(Double feeBaseAmount) {
		this.feeBaseAmount = feeBaseAmount;
	}

	public Integer getIsLatefeeApplicable() {
		return isLatefeeApplicable;
	}

	public void setIsLatefeeApplicable(Integer isLatefeeApplicable) {
		this.isLatefeeApplicable = isLatefeeApplicable;
	}

	public BeanLateFee getLatefee() {
		return latefee;
	}

	public void setLatefee(BeanLateFee latefee) {
		this.latefee = latefee;
	}

	public BeanFeeLookup getFeeLookup() {
		return feeLookup;
	}

	public void setFeeLookup(BeanFeeLookup feeLookup) {
		this.feeLookup = feeLookup;
	}
	
	
}
