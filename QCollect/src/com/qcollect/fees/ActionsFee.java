package com.qcollect.fees;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.opensymphony.xwork2.ActionSupport;
import com.qcollect.formbuilder.ActionsFormBuilder;
public class ActionsFee extends ActionSupport {

	HttpServletRequest request = ServletActionContext.getRequest();
	HttpSession ses = request.getSession();
	static Logger log = Logger.getLogger(ActionsFee.class.getName());
	private DaoFee daofee=new DaoFee();
	private BeanFeeLookup feeLookup=new BeanFeeLookup();
	
	public String saveFeeLookup()
	{
		BeanFeeLookup beanFeeLU=new BeanFeeLookup();
		try
		{
			//Uncomment Below Code After Login Flow is Done
			/*Integer ownerId=(Integer)ses.getAttribute("sesUserID");
			feeLookup.setFeeOwnerId(ownerId);*/
			beanFeeLU=daofee.saveFeeLookup(feeLookup);
			
			if(beanFeeLU.getId()!=null)
			{
				log.info("Fee Successfully Saved with ID: "+beanFeeLU.getId());
			}
			else
			{
				log.info("Fee NOT saved!");
			}
			return SUCCESS;
		}
		finally
		{
			
		}
	}

	public BeanFeeLookup getFeeLookup() {
		return feeLookup;
	}

	public void setFeeLookup(BeanFeeLookup feeLookup) {
		this.feeLookup = feeLookup;
	}
	
	
	
}
