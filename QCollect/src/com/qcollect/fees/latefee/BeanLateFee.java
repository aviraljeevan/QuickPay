package com.qcollect.fees.latefee;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;


@Entity
@Table(name = "data_latefee")
public class BeanLateFee {

	@GenericGenerator(name = "g1", strategy = "increment")
	@Id
	@GeneratedValue(generator = "g1")
	private Integer id;
	private String latefeeType;
	private Double latefeeAmount;
	private String latefeeFormula;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getLatefeeType() {
		return latefeeType;
	}
	public void setLatefeeType(String latefeeType) {
		this.latefeeType = latefeeType;
	}
	public Double getLatefeeAmount() {
		return latefeeAmount;
	}
	public void setLatefeeAmount(Double latefeeAmount) {
		this.latefeeAmount = latefeeAmount;
	}
	public String getLatefeeFormula() {
		return latefeeFormula;
	}
	public void setLatefeeFormula(String latefeeFormula) {
		this.latefeeFormula = latefeeFormula;
	}
	
	
}
