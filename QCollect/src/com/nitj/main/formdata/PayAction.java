package com.nitj.main.formdata;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Order;

import com.opensymphony.xwork2.ActionSupport;
import com.qcollect.main.ConnectionClass;

public class PayAction extends ActionSupport {

	HttpServletRequest request = ServletActionContext.getRequest();
	HttpServletResponse response = ServletActionContext.getResponse();

	HttpSession httpSession = ServletActionContext.getRequest().getSession();
	public static SessionFactory factory = ConnectionClass.getFactory();
	private static final long serialVersionUID = 545455799825524368L;
	public FormData fd;
	static Logger log = Logger.getLogger(PayAction.class.getName());

	public String responseHandelling() {

		log.info("responce Has COme");

		return SUCCESS;
	}

	public String PayFee() {
		// Declarations
		fd.setTxnDate(new Date());
		fd.setTxnStatus("NA");
		// Generate Transaction ID
		Integer rowCount = getRowCount();
		int fillercount = 10 - rowCount.toString().length();
		String txnStr = rowCount.toString();
		for (int i = 0; i < fillercount; i++) {
			txnStr = txnStr.concat("0");
		}
		fd.setTransId(Long.parseLong(txnStr.trim()));

		// --
		// Open session from session factory
		Session session = factory.openSession();
		try {
			session.beginTransaction();
			session.saveOrUpdate(fd);
			session.getTransaction().commit();
			String password = "%nit15$#@";

			PasswordEncryption.encrypt(password);
			String encryptedPwd = PasswordEncryption.encStr;
			httpSession.setAttribute("encPwd", encryptedPwd);
			return SUCCESS;

		} catch (Exception e) {
			e.printStackTrace();
			return ERROR;

		} finally {
			// close session
			session.close();
		}

	}

	public Integer getRowCount() {
		// Declarations

		// Open session from session factory
		Session session = factory.openSession();
		try {
			Criteria c = session.createCriteria(FormData.class);
			c.addOrder(Order.desc("id"));
			c.setMaxResults(1);
			FormData temp = (FormData) c.uniqueResult();

			try {
				return temp.getId() + 1;
			} catch (java.lang.NullPointerException e) {
				return 0;
			}

		} finally {
			// close session
			session.close();
		}

	}

	public FormData getFd() {
		return fd;
	}

	public void setFd(FormData fd) {
		this.fd = fd;
	}

}
