package com.dexpert.qcollect.applicant;

import java.util.List;

import org.apache.log4j.Logger;
import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Projections;

import com.dexpert.qcollect.users.operator.OperatorDao;
import com.qcollect.demo.SampleFormBean;
import com.qcollect.main.ConnectionClass;

public class ApplicantDAO {
	public static SessionFactory factory = ConnectionClass.getFactory();
	static Logger log = Logger.getLogger(OperatorDao.class.getName());

	public List<String> getAllAplicantDetails() {
		Session session = factory.openSession();
        Criteria criteria=session.createCriteria(SampleFormBean.class);
        criteria.setProjection(Projections.property("formData"));
       List<String> applicantDetails= criteria.list();
       return applicantDetails;
	}

}
