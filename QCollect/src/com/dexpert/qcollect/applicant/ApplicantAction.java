package com.dexpert.qcollect.applicant;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import javax.servlet.http.HttpServletRequest;

import org.apache.log4j.Logger;
import org.apache.struts2.ServletActionContext;

import com.dexpert.qcollect.users.operator.OperatorAction;
import com.opensymphony.xwork2.ActionSupport;
public class ApplicantAction extends ActionSupport{
	ApplicantDAO applicantDAO = new ApplicantDAO();
    Set<String> headerSet;
    static Logger log = Logger.getLogger(ApplicantAction.class.getName());
    ArrayList<ArrayList<String>> aplDetails;
    HttpServletRequest request=ServletActionContext.getRequest();
	// method to find of the meta data of applicant and return in the form of
	// set
	public Set<String> getHeader(String values) {
		// applicant value is seperated by (,) value is being spited
		String[] aplicantKeyValueArray = values.split(",");
		// creating linkedHash set for stroing the key only with coming order
		Set<String> keySet = new LinkedHashSet<String>();
		for (int i = 0; i < aplicantKeyValueArray.length; i++) {
			// after spited string got the key value Pair
			String keyValuePair = aplicantKeyValueArray[i];
			// again spited key value pair string and get the key and add into
			// set
			
			String key = keyValuePair.substring(0, keyValuePair.lastIndexOf("="));
			key = key.replaceAll("[^A-Za-z\\s]", "");
			keySet.add(key);
		}

		return keySet;

	}

	public ArrayList<ArrayList<String>> getValueOfApplicant(List<String> allApplicantDetails) {
		ArrayList<ArrayList<String>> allApplicantData = new ArrayList<ArrayList<String>>();
		Iterator<String> aplItr = allApplicantDetails.iterator();
		while (aplItr.hasNext()) {
			ArrayList<String> singleApplicantDetail = new ArrayList<String>();
			String oneAplicantDetails = aplItr.next();
			String keyValuePairArray[] = oneAplicantDetails.split(",");
			for (int i = 0; i < keyValuePairArray.length; i++) {
				String onePairOfKeyValue = keyValuePairArray[i];
				String value = onePairOfKeyValue.substring(onePairOfKeyValue.lastIndexOf("=") + 1);
				singleApplicantDetail.add(value);
			}
			allApplicantData.add(singleApplicantDetail);
		}
		return allApplicantData;
	}

	public String viewAllApplicant() {
		String forDownLoad=request.getParameter("ForDownLoad");
		List<String> tempApplicantDetails = applicantDAO.getAllAplicantDetails();
		headerSet=getHeader(tempApplicantDetails.get(0));
		aplDetails=getValueOfApplicant(tempApplicantDetails);
		String result=forDownLoad!=null&&forDownLoad.contentEquals("True")?"ForDown":"success";
		return result;
	}

	
	
	//setters and getters
	public Set<String> getHeaderSet() {
		return headerSet;
	}

	public void setHeaderSet(Set<String> headerSet) {
		this.headerSet = headerSet;
	}

	public ArrayList<ArrayList<String>> getAplDetails() {
		return aplDetails;
	}

	public void setAplDetails(ArrayList<ArrayList<String>> aplDetails) {
		this.aplDetails = aplDetails;
	}
	
	
	
	

}
