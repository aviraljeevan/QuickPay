package com.dexpert.qcollect.college;

import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;

import org.hibernate.annotations.GenericGenerator;

import com.dexpert.qcollect.main.users.LoginBean;
import com.qcollect.demo.SampleTransBean;

@Entity
@Table(name = "college_master")
public class CollegeBean {


	@GenericGenerator(name = "g1", strategy = "increment")
	@Id
	@GeneratedValue(generator = "g1")
	private Integer collegeId;
	//required for receipt generation
	private String collegeCode;
	private String collegeName; 
	
	@OneToOne(cascade = CascadeType.ALL)
	LoginBean loginBean;
	
	
	public LoginBean getLoginBean() {
		return loginBean;
	}
	public void setLoginBean(LoginBean loginBean) {
		this.loginBean = loginBean;
	}
	//one college can have multiple transaction managing relationship for that
	@OneToMany(fetch=FetchType.LAZY,cascade=CascadeType.ALL,mappedBy="collegeBean")
	
	Set<SampleTransBean> transactions;
	public Integer getCollegeId() {
		return collegeId;
	}
	public void setCollegeId(Integer collegeId) {
		this.collegeId = collegeId;
	}
	public String getCollegeCode() {
		return collegeCode;
	}
	public void setCollegeCode(String collegeCode) {
		this.collegeCode = collegeCode;
	}
	public String getCollegeName() {
		return collegeName;
	}
	public void setCollegeName(String collegeName) {
		this.collegeName = collegeName;
	}
	public Set<SampleTransBean> getTransactions() {
		return transactions;
	}
	public void setTransactions(Set<SampleTransBean> transactions) {
		this.transactions = transactions;
	}
	
	
}
