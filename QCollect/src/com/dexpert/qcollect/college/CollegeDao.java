package com.dexpert.qcollect.college;

import org.apache.log4j.Logger;
import org.hibernate.Session;
import org.hibernate.SessionFactory;

import com.qcollect.main.ConnectionClass;

public class CollegeDao {

	public static SessionFactory factory = ConnectionClass.getFactory();
	static Logger log = Logger.getLogger(CollegeDao.class.getName());
	
	
	
	
	
	public static CollegeBean viewInstDetail(Integer collegeId) {
		log.info("before creating session");
		Session session = factory.openSession();
		log.info("after creating session");
		CollegeBean cBean  = (CollegeBean)session.get(CollegeBean.class,collegeId);
	    log.info("after fetching bean");
		session.close();
		return cBean;
	}
}
