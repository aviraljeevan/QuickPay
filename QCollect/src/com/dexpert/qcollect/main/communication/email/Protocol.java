package com.dexpert.qcollect.main.communication.email;

public enum Protocol {

	SMTP,
	SMTPS,
	TLS
}
