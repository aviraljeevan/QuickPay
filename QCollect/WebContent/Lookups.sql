/*
-- Query: SELECT * FROM qcollect.lookup_form_fields
LIMIT 0, 1000

-- Date: 2015-11-03 01:10
*/
INSERT INTO `lookup_form_fields` (`lookup_id`,`lookup_name`,`lookup_type`,`isPredefined`,`lookup_subtype`) VALUES (1,'Name of Student','Input',1,'Text');
INSERT INTO `lookup_form_fields` (`lookup_id`,`lookup_name`,`lookup_type`,`isPredefined`,`lookup_subtype`) VALUES (2,'Name of Father','Input',1,'Text');
INSERT INTO `lookup_form_fields` (`lookup_id`,`lookup_name`,`lookup_type`,`isPredefined`,`lookup_subtype`) VALUES (3,'Gender','Select Box',1,'NA');
INSERT INTO `lookup_form_fields` (`lookup_id`,`lookup_name`,`lookup_type`,`isPredefined`,`lookup_subtype`) VALUES (4,'Subject','Select Box',1,'NA');
INSERT INTO `lookup_form_fields` (`lookup_id`,`lookup_name`,`lookup_type`,`isPredefined`,`lookup_subtype`) VALUES (5,'Hostel Required','Select Box',1,'NA');
INSERT INTO `lookup_form_fields` (`lookup_id`,`lookup_name`,`lookup_type`,`isPredefined`,`lookup_subtype`) VALUES (6,'Enrollment Number','Input',1,'Number');
INSERT INTO `lookup_form_fields` (`lookup_id`,`lookup_name`,`lookup_type`,`isPredefined`,`lookup_subtype`) VALUES (7,'Name of Mother','Input',1,'Text');
INSERT INTO `lookup_form_fields` (`lookup_id`,`lookup_name`,`lookup_type`,`isPredefined`,`lookup_subtype`) VALUES (8,'Mobile Number','Input',1,'Number');
INSERT INTO `lookup_form_fields` (`lookup_id`,`lookup_name`,`lookup_type`,`isPredefined`,`lookup_subtype`) VALUES (9,'Date of Birth','Input',1,'Date');
INSERT INTO `lookup_form_fields` (`lookup_id`,`lookup_name`,`lookup_type`,`isPredefined`,`lookup_subtype`) VALUES (10,'Address','Input',1,'Text');
INSERT INTO `lookup_form_fields` (`lookup_id`,`lookup_name`,`lookup_type`,`isPredefined`,`lookup_subtype`) VALUES (11,'Email','Input',1,'Email');
