<!DOCTYPE html>
<html lang="en">
<%@ taglib prefix="s" uri="/struts-tags"%>
<head>

<meta charset="utf-8">
<title>QuickCollect</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The styles -->
<link id="bs-css" href="css/bootstrap-cerulean.min.css" rel="stylesheet">

<link href="css/charisma-app.css" rel="stylesheet">
<link href='bower_components/fullcalendar/dist/fullcalendar.css'
	rel='stylesheet'>
<link href='bower_components/fullcalendar/dist/fullcalendar.print.css'
	rel='stylesheet' media='print'>
<link href='bower_components/chosen/chosen.min.css' rel='stylesheet'>
<link href='bower_components/colorbox/example3/colorbox.css'
	rel='stylesheet'>
<link href='bower_components/responsive-tables/responsive-tables.css'
	rel='stylesheet'>
<link
	href='bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css'
	rel='stylesheet'>
<link href='css/jquery.noty.css' rel='stylesheet'>
<link href='css/noty_theme_default.css' rel='stylesheet'>
<link href='css/elfinder.min.css' rel='stylesheet'>
<link href='css/elfinder.theme.css' rel='stylesheet'>
<link href='css/jquery.iphone.toggle.css' rel='stylesheet'>
<link href='css/uploadify.css' rel='stylesheet'>
<link href='css/animate.min.css' rel='stylesheet'>

<!-- jQuery -->
<script src="bower_components/jquery/jquery.min.js"></script>

<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

<!-- The fav icon -->
<link rel="shortcut icon" href="img/favicon.ico">

</head>

<body>


	<table class="table table-striped table-condensed datatable"
		id="mainForm1">
		<thead>
			<tr>
				<th>Sr. No.</th>
				<th>Fee Name</th>
				<th>Amount Paid</th>
				<th>Transaction Id</th>
				<th>Transaction Date</th>
				<th>Transaction Type</th>
				<th>Payee Name</th>
				<th>Status</th>
				<!-- <th>Actions <a href=""> <img src="img/Xcel-icon.jpg">
				</a> -->




			</tr>
		</thead>
		<s:iterator value="payerHistory"><!-- transData payerHistory-->
			<tr>
				<td><s:property value="id" /></td>

				<td><s:property value="feeNames" /></td>
				<td><s:property value="transAmount" /></td>
				<td><s:property value="transId" />
				<input type="hidden" id="tranId" value='<s:property value="transId" />'>
				</td>
				<td><s:property value="transDate" /></td>
				<td><s:property value="transPaymode" /></td>
				<td><s:property value="name" /></td>
				<td><s:property value="transStatus" /></td>
				<td><button type="button" onclick="viewReceipt()"
						class="btn btn-success btn-sm">View Receipt</button></td>
			</tr>
		</s:iterator>

	</table>


<script type="text/javascript">

function viewReceipt() {
	alert("inside recipt print");
	
	var tid=document.getElementById("tranId").value;
	alert(tid);
	/* window.open("Receipt.jsp", "Preview", "width=500,height=768"); */
	window.location="viewReceiptDetails?tid="+tid;
}

</script>




</body>
</html>
