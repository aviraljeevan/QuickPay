<html lang="en">
<head>

<meta charset="utf-8">
<title>QuickCollect</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The styles -->
<link id="bs-css" href="css/bootstrap-cerulean.min.css" rel="stylesheet">

<link href="css/charisma-app.css" rel="stylesheet">
<link href='bower_components/fullcalendar/dist/fullcalendar.css'
	rel='stylesheet'>
<link href='bower_components/fullcalendar/dist/fullcalendar.print.css'
	rel='stylesheet' media='print'>
<link href='bower_components/chosen/chosen.min.css' rel='stylesheet'>
<link href='bower_components/colorbox/example3/colorbox.css'
	rel='stylesheet'>
<link href='bower_components/responsive-tables/responsive-tables.css'
	rel='stylesheet'>
<link
	href='bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css'
	rel='stylesheet'>
<link href='css/jquery.noty.css' rel='stylesheet'>
<link href='css/noty_theme_default.css' rel='stylesheet'>
<link href='css/elfinder.min.css' rel='stylesheet'>
<link href='css/elfinder.theme.css' rel='stylesheet'>
<link href='css/jquery.iphone.toggle.css' rel='stylesheet'>
<link href='css/uploadify.css' rel='stylesheet'>
<link href='css/animate.min.css' rel='stylesheet'>

<!-- jQuery -->
<script src="bower_components/jquery/jquery.min.js"></script>

<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

<!-- The fav icon -->
<link rel="shortcut icon" href="img/favicon.ico">

</head>

<body>
	<!-- topbar starts -->
	<div style="display: none" class="navbar navbar-default" role="navigation" id="topbarCHSE">

		<div class="navbar-inner">
			<button type="button" class="navbar-toggle pull-left">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			



		</div>
	</div>
	
	


		


			<noscript>
				<div class="alert alert-block col-md-12">
					<h4 class="alert-heading">Warning!</h4>

					<p>
						You need to have <a href="http://en.wikipedia.org/wiki/JavaScript"
							target="_blank">JavaScript</a> enabled to use this site.
					</p>
				</div>
			</noscript>

			<div id="content" class="col-lg-12 col-sm-12">
				<!-- content starts -->




				<div class="row">
					<div class="box col-md-12">
						<div class="box-inner">
							<div class="box-header well" data-original-title="">
								<h2>
									<i class="glyphicon glyphicon-star-empty"></i> Fee Payment Form
								</h2>

								<div class="box-icon">
									<a href="#" class="btn btn-minimize btn-round btn-default"><i
										class="glyphicon glyphicon-chevron-up"></i></a>
								</div>
							</div>
							<div class="box-content">

<table>
										<tr>
											<td colspan="2">
												<div class="control-group">
													<label class="control-label" for="selectError">Select
														Institute</label>

													<div class="controls">
														<select onchange="makeGetRequest(this.value),getValue(this.value)" name="fd.paymentCat" id="selectInst"
															data-rel="chosen" required="required">
															<option selected="selected" disabled="disabled">Select
																An Option</option>
															<option value="1">Council Of Higher Secondary
																Education, Odisha</option>
																<option value="2">National Institute of
																Technology, Jalandhar</option>


														</select>
													</div>
												</div>
											</td>
										</tr>
										</table>

									<div id="selected_College"></div>
<td>
			<button type="button" id="submit_button" class="btn btn-success">Pay</button></td>
			<td><button class="btn btn-default" onclick="Reset()">
													Reset</button></td>

<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

	<!-- library for cookie management -->
	<script src="js/jquery.cookie.js"></script>
	<!-- calender plugin -->
	<script src='bower_components/moment/min/moment.min.js'></script>
	<script src='bower_components/fullcalendar/dist/fullcalendar.min.js'></script>
	<!-- data table plugin -->
	<script src='js/jquery.dataTables.min.js'></script>

	<!-- select or dropdown enhancer -->
	<script src="bower_components/chosen/chosen.jquery.min.js"></script>
	<!-- plugin for gallery image view -->
	<script src="bower_components/colorbox/jquery.colorbox-min.js"></script>
	<!-- notification plugin -->
	<script src="js/jquery.noty.js"></script>
	<!-- library for making tables responsive -->
	<script src="bower_components/responsive-tables/responsive-tables.js"></script>
	<!-- tour plugin -->
	<script
		src="bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
	<!-- star rating plugin -->
	<script src="js/jquery.raty.min.js"></script>
	<!-- for iOS style toggle switch -->
	<script src="js/jquery.iphone.toggle.js"></script>
	<!-- autogrowing textarea plugin -->
	<script src="js/jquery.autogrow-textarea.js"></script>
	<!-- multiple file upload plugin -->
	<script src="js/jquery.uploadify-3.1.min.js"></script>
	<!-- history.js for cross-browser state change on ajax -->
	<script src="js/jquery.history.js"></script>
	<!-- application script for Charisma demo -->


<script type="text/javascript">

function createRequestObject() {
    var tmpXmlHttpObject;
    
    //depending on what the browser supports, use the right way to create the XMLHttpRequest object
    if (window.XMLHttpRequest) { 
        // Mozilla, Safari would use this method ...
        tmpXmlHttpObject = new XMLHttpRequest();
	
    } else if (window.ActiveXObject) { 
        // IE would use this method ...
        tmpXmlHttpObject = new ActiveXObject("Microsoft.XMLHTTP");
    }
    
    return tmpXmlHttpObject;
}

//call the above function to create the XMLHttpRequest object
var http = createRequestObject();

function makeGetRequest(wordId) {
	
	alert(" testing the coding :"+wordId);
	 var collegePage;
	 var wordId=wordId;
	 if(wordId==1){
		 /* collegePage="index.jsp"; */
		 http.open('get', 'demoForm.jsp?id=' + wordId);
		 alert(collegePage);
	 }else{
		 /* collegePage="nitjFeesPaymentForm.jsp"; */
		 http.open('get', 'nitjFeesPaymentForm.jsp?id=' + wordId);
		 alert(collegePage);
	 }
	
	
    //make a connection to the server ... specifying that you intend to make a GET request 
    //to the server. Specifiy the page name and the URL parameters to send
   /*  http.open('get', 'collegePage?id=' + wordId); */
	
    //assign a handler for the response
    http.onreadystatechange = processResponse;
	
    //actually send the request to the server
    http.send(null);
}

function processResponse() {
    //check if the response has been received from the server
    if(http.readyState == 4){
	
        //read and assign the response from the server
        var response = http.responseText;
		
        //do additional parsing of the response, if needed
		
        //in this case simply assign the response to the contents of the <div> on the page. 
        document.getElementById('selected_College').innerHTML = response;
		
        //If the server returned an error message like a 404 error, that message would be shown within the div tag!!. 
        //So it may be worth doing some basic error before setting the contents of the <div>
    }
}
var fee = 0;
var totfee = 0;
var hostel = 0;
var bus = 0;
var hostelfee = 0;
var busfee = 0;
function GetFees(x) {
	//alert("testing fee");
	/* document.getElementById("nitForm").reset(); */
	if (x == "BT3" || x == "MT3GEN" || x == "MB14GEN") {
		fee = 42450;
		hostel = 2500;
		bus = 1800;
	}
	if (x == "BT5" || x == "BT7") {
		fee = 24950;
		hostel = 2500;
		bus = 1800;
	}
	if (x == "MT15SC" || x == "MB15SC" || x == "MS15SC"
			|| x == "PD15SCFT" || x == "PD15SCPT") {
		fee = 12550;
		hostel = 2500;
		bus = 1800;
	}
	if (x == "MS14GEN" || x == "PDGEN") {
		fee = 14950;
		hostel = 2500;
		bus = 1800;
	}
	if (x == "MT14SC" || x == "MB14SC" || x == "MS14SC"
			|| x == "MT12SC" || x == "MT14SC") {
		fee = 7450;
		hostel = 2500;
		bus = 1800;
	}
	if (x == "MT12GENPT") {
		fee = 17450;
	}
	if (x == "MT14GENPT") {
		fee = 27450;
	}
	if (x == "BT1" || x == "MT15GEN" || x == "MB15GEN") {
		fee = 47550;
		hostel = 2500;
		bus = 1800;
	}
	if (x == "MS15GEN" || x == "PD15GENFT" || x == "PD15GENPT") {
		fee = 20050;
		hostel = 2500;
		bus = 1800;
	}
	document.getElementById("fee").value = fee;
//	alert("fee"+fee);
	//document.getElementById("total").value = fee;
	document.getElementById("total").value = fee;
	document.getElementById("hostelfee").value = 0;
	document.getElementById("busfee").value = 0;
	totfee = fee;

}

function TotalFee(y) {
	//	alert("In TotalFee ");
		if (y == "HostelYes") {
			hostelfee = hostel;
		}
		if (y == "HostelNo") {
			hostelfee = 0;
		}
		if (y == "BusYes") {
			busfee = bus;
		}
		if (y == "BusNo") {
			busfee = 0;
		}
		document.getElementById("hostelfee").value = hostelfee;
		document.getElementById("busfee").value = busfee;
		document.getElementById("total").value = fee + busfee + hostelfee;
	}
var values = {};

function AddToArray(value, name, id) {
	value = value.split(",").join("");
	value = value.split("`").join("");
	value = value.split("=").join("");
	values[id] = id + "`" + name + "=" + value;
	var total1=document.getElementById("total").value; 
	var id1="18";
	var name1="Total Amount";
	values[id1] = id1 + "`" + name1 + "=" + total1;
	
	
	/* alert(JSON.stringify(values)); */

}

function formSubmit() {
	
	/*  var inst_id = document.getElementById("1").value;
	var fee_id = document.getElementById("7").value;  
*/

	
	var dataArray = new Array;
	for ( var value in values) {
		dataArray.push(values[value]);
	}
	var argument = "values=" + dataArray;/*  + "&instId=" + inst_id
			+ "&feeId=" + fee_id;  */
	window.location = "processForm?"+argument;
			


}
 $("#submit_button").click(function(){ 
	 var emailI=$("#224").val();
	 var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
	 var mobNum= $("#16").val();
	 var patternMobNum = /^[\s()+-]*([0-9][\s()+-]*){6,20}$/;
	 var patternSem = /^\[0-9]+$/;
	 var sem=$("#6").val();
	 var rollN=$("#2").val();
	
	  if ($("#1").val() == ""&&$("#222").val() == ""&&$("#223").val() == ""&&$("#4").val() == ""&&$("#55").val() == ""&&$("#77").val() == ""&&$("#88").val() == ""&&$("#13").val() == ""&&$("#14").val() == ""){
	    alert('please fill the required field')
	    return false;
	  } else if ($("#6").val() == ""||sem.length != 1 || sem.match(patternSem )){
		    alert('please fill the vaild Semester')
		    return false;
		  }
 else if ($("#2").val() == ""){
	    alert('please fill the vaild Roll Number')
	    return false;
	  }
	  
	  
	  else if ($("#15").val() == ""){
	    alert('please fill the required field')
	    return false;
	  }
	  else if ($("#16").val() == ""|| mobNum.length != 10 || !mobNum.match(patternMobNum) ){
		    alert('please fill the valid Mobile Number')
		    return false;
		  }
	  else if (!emailI.match(emailReg)||$("#224").val() == ""){
		    alert('please fill the Email Id')
		    return false;
		  }
	  else
		  
		  formSubmit();
	   
	});
 function viewHistory() {
		window
				.open("payer-History.jsp", "Preview",
						"width=1024,height=768");
	}





</script>



</body>
</html>