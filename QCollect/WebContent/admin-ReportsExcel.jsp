<!DOCTYPE html>
<html lang="en">
<%@ taglib prefix="s" uri="/struts-tags"%>
<head>
</head>

<body>
    <%response.setContentType("application/vnd.ms-excel"); 
    response.setHeader("Content-Disposition", "inline; filename=" + "Transactions.xls");
    %>
	<table class="table table-striped table-condensed datatable"
		id="mainForm1">
		<thead>
			<tr>
				<th>Sr. No.</th>
				<th>Fee Name</th>
				<th>Amount Paid</th>
				<th>Transaction Id</th>
				<th>Transaction Date</th>
				<th>Transaction Type</th>
				<th>Payee Name</th>
				<th>Status</th>
			</tr>
		</thead>
		<s:iterator value="TransData">
			<tr>
				<td><s:property value="id" /></td>
				<td>Readmission in Correspondence Course - 2nd year</td>
				<td><s:property value="transAmount" /></td>
				<td><s:property value="transId" /></td>
				<td><s:property value="transDate" /></td>
				<td><s:property value="transPaymode" /></td>
				<td>Mr. Kumar Manish</td>
				<td><s:property value="transStatus" /></td>
				
			</tr>
		</s:iterator>

	</table>







</body>
</html>
