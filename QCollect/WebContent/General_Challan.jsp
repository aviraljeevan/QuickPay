<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.0 Transitional//EN">

<!-- saved from url=(0052)http://www.verypdf.com/ -->
<html>
<head>
<title>SRS Challan</title>

<style>
<!--
select {
	font-size: 12px;
}

A:link {
	text-decoration: none;
	color: blue
}

A:visited {
	text-decoration: none;
	color: purple
}

A:active {
	text-decoration: red
}

A:hover {
	text-decoration: underline;
	color: red
}
-->
</style>
<script TYPE="text/javascript">
<!-- hide
	function killerrors() {
		return true;
	}
	window.onerror = killerrors;
// -->
</script>
<style type="text/css">
<!--
.ft0 {
	font-style: normal;
	font-weight: normal;
	font-size: 13px;
	font-family: Times New Roman;
	color: #000000;
}
-->
</style>
</head>


<script type="text/javascript">
	function submitparent() {

		window.opener.document.getElementById('sabpaisaCashIndex').submit();
	}
</script>


<body onload="submitparent()" vlink="#FFFFFF" link="#FFFFFF"
	bgcolor="#ffffff">

	<script TYPE="text/javascript">
		var currentpos, timer;
		function initialize() {
			timer = setInterval("scrollwindow()", 10);
		}
		function sc() {
			clearInterval(timer);
		}
		function scrollwindow() {
			currentpos = document.body.scrollTop;
			window.scroll(0, ++currentpos);
			if (currentpos != document.body.scrollTop)
				sc();
		}
		document.onmousedown = sc
		document.ondblclick = initialize
	</script>
	<div style="position: absolute; top: 0; left: 0">
		<img width="850" height="1100" src="img/pg_0001.jpg" ALT="">
	</div>
	<div style="position: absolute; top: 52; left: 67">
		<span class="ft0"> </span>
	</div>
	<div style="position: absolute; top: 70; left: 203">
		<span class="ft0"> Applicant copy</span>
	</div>
	<div style="position: absolute; top: 170; left: 200">
		<span class="ft0">H C O R E F E E</span>
	</div>
	<div style="position: absolute; top: 199; left: 110">
		<span class="ft0">Cash can be tendered at any Branch of ICICI Bank</span>
	</div>
	<div style="position: absolute; top: 226; left: 93">
		<span class="ft0">Institution Name</span>
	</div>
	<div style="position: absolute; top: 218; left: 233">
		<span class="ft0"> Spectra Research Services</span>
	</div>
	<div style="position: absolute; top: 233; left: 233">
		<span class="ft0">(SRS)</span>
	</div>
	<div style="position: absolute; top: 253; left: 93">
		<span class="ft0">Institution Branch</span>
	</div>
	<div style="position: absolute; top: 273; left: 93">
		<span class="ft0"> </span>
	</div>
	<div style="position: absolute; top: 273; left: 233">
		<span class="ft0">  </span>
	</div>
	<div style="position: absolute; top: 294; left: 93">
		<span class="ft0">Unique Reference No <span
			style="margin-left: 30px;"></span> <s:property
				value="challanBean.Challan_no" /></span>
	</div>
	<div style="position: absolute; top: 314; left: 93">
		<span class="ft0"> </span>
	</div>
	<div style="position: absolute; top: 314; left: 233">
		<span class="ft0">  </span>
	</div>
	<div style="position: absolute; top: 334; left: 93">
		<span class="ft0">Rollno./ Application No<span
			style="margin-left: 10px;"></span></span>
	</div>
	<div style="position: absolute; top: 354; left: 93">
		<span class="ft0">Payer Name</span>
	</div>
	<div style="position: absolute; top: 354; left: 233">
		<span class="ft0"><span style="margin-left: 30px;"></span></span>
	</div>
	<div style="position: absolute; top: 374; left: 93">
		<span class="ft0">Additional Field1</span>
	</div>
	<div style="position: absolute; top: 374; left: 233">
		<span class="ft0"> NA</span>
	</div>
	<div style="position: absolute; top: 394; left: 93">
		<span class="ft0">Additional Field2</span>
	</div>
	<div style="position: absolute; top: 394; left: 233">
		<span class="ft0"> NA</span>
	</div>
	<div style="position: absolute; top: 414; left: 93">
		<span class="ft0">Mobile No</span>
	</div>
	<div style="position: absolute; top: 414; left: 233">
		<span class="ft0"><span style="margin-left: 10px;"></span></span>
	</div>
	<div style="position: absolute; top: 434; left: 93">
		<span class="ft0"> </span>
	</div>
	<div style="position: absolute; top: 434; left: 233">
		<span class="ft0">  </span>
	</div>
	<div style="position: absolute; top: 454; left: 93">
		<span class="ft0">Amount to be paid (in</span>
	</div>
	<div style="position: absolute; top: 470; left: 93">
		<span class="ft0">Rs.)</span>
	</div>
	<div style="position: absolute; top: 462; left: 233">
		<span class="ft0"><span style="margin-left: 30px;"></span> </span>
	</div>
	<div style="position: absolute; top: 490; left: 93">
		<span class="ft0"> </span>
	</div>
	<div style="position: absolute; top: 523; left: 93">
		<span class="ft0"> </span>
	</div>
	<div style="position: absolute; top: 552; left: 93">
		<span class="ft0"> </span>
	</div>
	<div style="position: absolute; top: 580; left: 264">
		<span class="ft0"> Signature of Depositor  </span>
	</div>
	<div style="position: absolute; top: 728; left: 245">
		<span class="ft0"> </span>
	</div>
	<div style="position: absolute; top: 757; left: 89">
		<span class="ft0">To be Filled By Bank</span>
	</div>
	<div style="position: absolute; top: 790; left: 93">
		<span class="ft0">ICICI Branch Name</span>
	</div>
	<div style="position: absolute; top: 810; left: 93">
		<span class="ft0">Sol Id</span>
	</div>
	<div style="position: absolute; top: 830; left: 93">
		<span class="ft0">Cash Tran ID</span>
	</div>
	<div style="position: absolute; top: 850; left: 93">
		<span class="ft0">Deposit Date</span>
	</div>
	<div style="position: absolute; top: 883; left: 245">
		<span class="ft0">  </span>
	</div>
	<div style="position: absolute; top: 911; left: 89">
		<span class="ft0">Signature of Bank Official </span>
	</div>
	<div style="position: absolute; top: 926; left: 89">
		<span class="ft0">Branch Stamp</span>
	</div>
	<div style="position: absolute; top: 955; left: 89">
		<span class="ft0"> </span>
	</div>
	<div style="position: absolute; top: 70; left: 591">
		<span class="ft0"> Bank copy</span>
	</div>
	<div style="position: absolute; top: 170; left: 575">
		<span class="ft0">H C O R E F E E</span>
	</div>
	<div style="position: absolute; top: 199; left: 485">
		<span class="ft0">Cash can be tendered at any Branch of ICICI Bank</span>
	</div>
	<div style="position: absolute; top: 226; left: 468">
		<span class="ft0">Institution Name</span>
	</div>
	<div style="position: absolute; top: 218; left: 608">
		<span class="ft0"> Spectra Research Services</span>
	</div>
	<div style="position: absolute; top: 233; left: 608">
		<span class="ft0">(SRS)</span>
	</div>
	<div style="position: absolute; top: 253; left: 468">
		<span class="ft0">Institution Branch</span>
	</div>
	<div style="position: absolute; top: 273; left: 468">
		<span class="ft0"> </span>
	</div>
	<div style="position: absolute; top: 273; left: 608">
		<span class="ft0">  </span>
	</div>
	<div style="position: absolute; top: 294; left: 468">
		<span class="ft0">Unique Reference No <span
			style="margin-left: 30px;"></span> <s:property
				value="challanBean.Challan_no" /></span>
	</div>
	<div style="position: absolute; top: 314; left: 468">
		<span class="ft0"> </span>
	</div>
	<div style="position: absolute; top: 314; left: 608">
		<span class="ft0">  </span>
	</div>
	<div style="position: absolute; top: 334; left: 468">
		<span class="ft0">Rollno./ Application No<span
			style="margin-left: 10px;"></span></span>
	</div>
	<div style="position: absolute; top: 354; left: 468">
		<span class="ft0">Payer Name</span>
	</div>
	<div style="position: absolute; top: 354; left: 608">
		<span class="ft0"><span style="margin-left: 30px;"></span></span>
	</div>
	<div style="position: absolute; top: 374; left: 468">
		<span class="ft0">Additional Field1</span>
	</div>
	<div style="position: absolute; top: 374; left: 608">
		<span class="ft0"> NA</span>
	</div>
	<div style="position: absolute; top: 394; left: 468">
		<span class="ft0">Additional Field2</span>
	</div>
	<div style="position: absolute; top: 394; left: 608">
		<span class="ft0"> NA</span>
	</div>
	<div style="position: absolute; top: 414; left: 468">
		<span class="ft0">Mobile No</span>
	</div>
	<div style="position: absolute; top: 414; left: 608">
		<span class="ft0"><span style="margin-left: 10px;"></span></span>
	</div>
	<div style="position: absolute; top: 434; left: 468">
		<span class="ft0"> </span>
	</div>
	<div style="position: absolute; top: 434; left: 608">
		<span class="ft0">  </span>
	</div>
	<div style="position: absolute; top: 454; left: 468">
		<span class="ft0">Amount to be paid (in</span>
	</div>
	<div style="position: absolute; top: 470; left: 468">
		<span class="ft0">Rs.)</span>
	</div>
	<div style="position: absolute; top: 462; left: 608">
		<span class="ft0"><span style="margin-left: 30px;"></span> </span>
	</div>
	<div style="position: absolute; top: 490; left: 468">
		<span class="ft0"> </span>
	</div>
	<div style="position: absolute; top: 523; left: 468">
		<span class="ft0"> </span>
	</div>
	<div style="position: absolute; top: 552; left: 468">
		<span class="ft0"> </span>
	</div>
	<div style="position: absolute; top: 580; left: 635">
		<span class="ft0"> Signature of Depositor   </span>
	</div>
	<div style="position: absolute; top: 641; left: 464">
		<span class="ft0">Instructions for ICICI Bank Branches</span>
	</div>
	<div style="position: absolute; top: 656; left: 464">
		<span class="ft0">1. This is an I Sure Pay Challan &amp; Institution</span>
	</div>
	<div style="position: absolute; top: 671; left: 464">
		<span class="ft0">Code/Account No. is not required. </span>
	</div>
	<div style="position: absolute; top: 687; left: 464">
		<span class="ft0">2. Type 'HCOREFEE' in finacle &gt; Select 'Institution name'</span>
	</div>
	<div style="position: absolute; top: 702; left: 464">
		<span class="ft0">&gt; Enter 'Unique reference no.' &gt; Click Submit &gt; Verify</span>
	</div>
	<div style="position: absolute; top: 717; left: 464">
		<span class="ft0">details &gt; Accept cash &gt; Click Submit &gt; Generate Receipt</span>
	</div>
	<div style="position: absolute; top: 732; left: 464">
		<span class="ft0">&gt; Handover to Depositor.</span>
	</div>
	<div style="position: absolute; top: 760; left: 464">
		<span class="ft0">To be Filled By Bank</span>
	</div>
	<div style="position: absolute; top: 793; left: 468">
		<span class="ft0">ICICI Branch Name</span>
	</div>
	<div style="position: absolute; top: 813; left: 468">
		<span class="ft0">Sol Id</span>
	</div>
	<div style="position: absolute; top: 833; left: 468">
		<span class="ft0">Cash Tran ID</span>
	</div>
	<div style="position: absolute; top: 853; left: 468">
		<span class="ft0">Deposit Date</span>
	</div>
	<div style="position: absolute; top: 886; left: 620">
		<span class="ft0">  </span>
	</div>
	<div style="position: absolute; top: 914; left: 464">
		<span class="ft0">Signature of Bank Official </span>
	</div>
	<div style="position: absolute; top: 930; left: 464">
		<span class="ft0">Branch Stamp</span>
	</div>
	<div style="position: absolute; top: 958; left: 464">
		<span class="ft0"> </span>
	</div>
	<div style="position: absolute; top: 52; left: 793">
		<span class="ft0">  </span>
	</div>
	<script TYPE="text/javascript">
		var currentZoom = parent.ltop.currentZoom;
		if (currentZoom != undefined)
			document.body.style.zoom = currentZoom / 100;
	</script>
</body>
</html>