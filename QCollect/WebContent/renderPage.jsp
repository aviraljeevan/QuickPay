<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<%@ taglib prefix="s" uri="/struts-tags"%>



<body>
	<table>
		
		<s:iterator value="renderLookups">
			<tr>
				<td><s:property value="lookup_name" /></td>
				<td><s:set var="fieldType">
						<s:property value="lookup_type" />
					</s:set> <s:set var="fieldSubtype">
						<s:property value="lookup_subtype" />
					</s:set> <s:if
						test='%{#fieldType.contentEquals("Input")&&#fieldSubtype.contentEquals("Number")}'>
						<input type="number">
					</s:if> <s:elseif
						test='%{#fieldType.contentEquals("Input")&&#fieldSubtype.contentEquals("Hidden")}'>
						<input type="password">
					</s:elseif> <s:elseif
						test='%{#fieldType.contentEquals("Input")&&#fieldSubtype.contentEquals("Text")}'>
						<input type="text">
					</s:elseif>
					<s:elseif
						test='%{#fieldType.contentEquals("Input")&&#fieldSubtype.contentEquals("Email")}'>
						<input type="email">
					</s:elseif>
					<s:elseif
						test='%{#fieldType.contentEquals("Input")&&#fieldSubtype.contentEquals("Date")}'>
						<input type="date">
					</s:elseif>
					 <s:elseif test='%{#fieldType.contentEquals("Select Box")}'>
						<s:set var="key">
							<s:property value="lookup_id" />
						</s:set>

						<select id='selectObj' name='selectObj'>


							<s:iterator value='optionsMap[#key]' var="map">
								<option><s:property /></option>
							</s:iterator>
						</select>


						
					</s:elseif></td><td> <input type="checkbox"  onchange="AddToMandatoryArray(this.id)"  name='mandatorycheck' id="<s:property value='lookup_id' />">Mandatory Field  </td>
			</tr>

		</s:iterator>

	</table>
</body>
</html>