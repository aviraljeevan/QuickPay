<!DOCTYPE html>
<%@page import="org.eclipse.jdt.internal.compiler.ast.ForeachStatement"%>
<%@page import="org.apache.jasper.tagplugins.jstl.core.ForEach"%>
<%@page import="java.util.ArrayList"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<%@page import="com.qcollect.demo.SampleTransBean"%>
<%@page import="com.qcollect.demo.SampleFormView"%>
<%@page import="java.util.Map" %>
<%@page import="java.util.HashMap" %>

<%@page import="java.util.Iterator" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="en">
<head>

<meta charset="utf-8">
<title>QuickCollect</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The styles -->
<link id="bs-css" href="css/bootstrap-cerulean.min.css" rel="stylesheet">

<link href="css/charisma-app.css" rel="stylesheet">
<link href='bower_components/fullcalendar/dist/fullcalendar.css'
	rel='stylesheet'>
<link href='bower_components/fullcalendar/dist/fullcalendar.print.css'
	rel='stylesheet' media='print'>
<link href='bower_components/chosen/chosen.min.css' rel='stylesheet'>
<link href='bower_components/colorbox/example3/colorbox.css'
	rel='stylesheet'>
<link href='bower_components/responsive-tables/responsive-tables.css'
	rel='stylesheet'>
<link
	href='bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css'
	rel='stylesheet'>
<link href='css/jquery.noty.css' rel='stylesheet'>
<link href='css/noty_theme_default.css' rel='stylesheet'>
<link href='css/elfinder.min.css' rel='stylesheet'>
<link href='css/elfinder.theme.css' rel='stylesheet'>
<link href='css/jquery.iphone.toggle.css' rel='stylesheet'>
<link href='css/uploadify.css' rel='stylesheet'>
<link href='css/animate.min.css' rel='stylesheet'>

<!-- jQuery -->
<script src="bower_components/jquery/jquery.min.js"></script>

<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

<!-- The fav icon -->
<link rel="shortcut icon" href="img/favicon.ico">

</head>

<body onload="Pay()"><!-- onload="Pay()" -->
	<!-- topbar starts -->
	
	
	<%-- <%
	
	
		ArrayList<SampleFormView> sfview=new ArrayList<SampleFormView>();
			
			sfview=(ArrayList<SampleFormView>)session.getAttribute("formViewData");
			
			
			%> --%>
	<div class="navbar navbar-default" role="navigation">

		<div class="navbar-inner">
			<button type="button" class="navbar-toggle pull-left">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="Login.jsp"> <img
				alt="Charisma Logo" src="img/logo_nitj.png"></a> <a
				class="navbar-brand" href="Login.jsp"><span>NITJ
					</span></a>
			<div class="btn-group pull-right">
				<!-- <button type="button" class="btn btn-default"
					onclick="viewHistory()">
					<i class="glyphicon glyphicon-time"></i> Previous Transactions
				</button> -->

			</div>




		</div>
	</div>
	<!-- topbar ends -->
	<div class="ch-container">
		<div class="row">


			<noscript>
				<div class="alert alert-block col-md-12">
					<h4 class="alert-heading">Warning!</h4>

					<p>
						You need to have <a href="http://en.wikipedia.org/wiki/JavaScript"
							target="_blank">JavaScript</a> enabled to use this site.
					</p>
				</div>
			</noscript>

			<div id="content" class="col-lg-12 col-sm-12">
				<!-- content starts -->




				<div class="row">
					<div class="box col-md-12">
						<div class="box-inner">
							<div class="box-header well" data-original-title="">
								<h2>
									<i class="glyphicon glyphicon-star-empty"></i> Transaction
									Summary
								</h2>

								<div class="box-icon">
									<a href="#" class="btn btn-minimize btn-round btn-default"><i
										class="glyphicon glyphicon-chevron-up"></i></a>
								</div>
							</div>
							<div class="box-content">
								<!-- put your content here -->

								<table class="table table-striped table-condensed">

									<tr>
										<td><strong>Transaction ID:</strong></td>
										<td><s:property value="beanTransData.transId" /></td>
									</tr>

									<tr>
										<td><strong>Transaction Date</strong></td>
										<td><s:property value="beanTransData.transDate" /></td>
									</tr>

									<tr>
										<td><strong>Transaction Status</strong></td>
										<td><s:property value="beanTransData.transStatus" /></td>
									</tr>
		
									<tr>
										<td>
											<button class="btn btn-sm btn-warning">Cancel</button>
										</td>
										<td>
											<button   class="btn btn-sm btn-success">Proceed</button>
										</td>
									</tr>
								</table>

							</div>
						</div>
					</div>
				</div>
				<!--/row-->


				<!-- content ends -->
			</div>
			<!--/#content.col-md-0-->
		</div>
		<!--/fluid-row-->


		<hr>

		<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel" aria-hidden="true">

			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">�</button>
						<h3>Settings</h3>
					</div>
					<div class="modal-body">
						<p>Here settings can be configured...</p>
					</div>
					<div class="modal-footer">
						<a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
						<a href="#" class="btn btn-primary" data-dismiss="modal">Save
							changes</a>
					</div>
				</div>
			</div>
		</div>
		<footer class="row">

			<div style="text-align: center;">

				<a href=""
					onclick="window.open('ContactUs.html','mywindowtitle',
											'width=500,height=550')">Contact
					Us </a> <span> |&nbsp;&nbsp;</span> <a href=""
					onclick="window.open('PrivacyPolicy.html','mywindowtitle',
											'width=500,height=550')">Privacy
					Policy </a> <span> |&nbsp;&nbsp;</span> <a href=""
					onclick="window.open('Terms And Conditions.html','mywindowtitle',
											'width=500,height=550')">Terms
					& Conditions </a> <span> |&nbsp;&nbsp;</span> <a href=""
					onclick="window.open('Disclaimer.html','mywindowtitle',
											'width=500,height=550')">Disclaimer
				</a>







			</div>


		</footer>
	</div>
	<!--/.fluid-container-->

	<!-- external javascript -->

	<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

	<!-- library for cookie management -->
	<script src="js/jquery.cookie.js"></script>
	<!-- calender plugin -->
	<script src='bower_components/moment/min/moment.min.js'></script>
	<script src='bower_components/fullcalendar/dist/fullcalendar.min.js'></script>
	<!-- data table plugin -->
	<script src='js/jquery.dataTables.min.js'></script>

	<!-- select or dropdown enhancer -->
	<script src="bower_components/chosen/chosen.jquery.min.js"></script>
	<!-- plugin for gallery image view -->
	<script src="bower_components/colorbox/jquery.colorbox-min.js"></script>
	<!-- notification plugin -->
	<script src="js/jquery.noty.js"></script>
	<!-- library for making tables responsive -->
	<script src="bower_components/responsive-tables/responsive-tables.js"></script>
	<!-- tour plugin -->
	<script
		src="bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
	<!-- star rating plugin -->
	<script src="js/jquery.raty.min.js"></script>
	<!-- for iOS style toggle switch -->
	<script src="js/jquery.iphone.toggle.js"></script>
	<!-- autogrowing textarea plugin -->
	<script src="js/jquery.autogrow-textarea.js"></script>
	<!-- multiple file upload plugin -->
	<script src="js/jquery.uploadify-3.1.min.js"></script>
	<!-- history.js for cross-browser state change on ajax -->
	<script src="js/jquery.history.js"></script>
	<!-- application script for Charisma demo -->
	<script src="js/charisma.js"></script>


		
	<%SampleTransBean transData = new SampleTransBean();
			transData = (SampleTransBean) session.getAttribute("sesCurrentTransData");
			Double transAmount = transData.getTransAmount();
			String transId=transData.getTransId();
			  Map<String,String> beanDetails=new HashMap<String,String>(); 
			 beanDetails=(HashMap<String,String>)session.getAttribute("dataMapDetails");
			
         ArrayList<SampleFormView> sfview=new ArrayList<SampleFormView>();
			
			sfview=(ArrayList<SampleFormView>)session.getAttribute("formViewData");
			
			 Iterator<SampleFormView> itr = sfview.iterator();
	          while (itr.hasNext()) {
	        	  SampleFormView formValues = (SampleFormView)itr.next();
	        	  
	          out.println("form label Value:"+formValues.getLabel());
	          out.println("form  Value:"+formValues.getValue());
	          beanDetails.put(formValues.getLabel(), formValues.getValue());
	          
	          }
			
	          System.out.println("hello testing value:"+beanDetails.get("Total Amount"));
			%>
			
			<input type="hidden"  id="course" value='<%=beanDetails.get("Branch")%>'>
			<input type="hidden"  id="RollNo" value='<%=beanDetails.get("Roll Number")%>'>
			<input type="hidden"  id="name1" value='<%=beanDetails.get("firstName")%>'>
			<input type="hidden"  id="amount" value='<%=transAmount.toString()%>'>
			<input type="hidden"  id="mobNum" value='<%=beanDetails.get("Mobile Number")%>'>
			<input type="hidden"  id="email" value='<%=beanDetails.get("Email")%>'>
			<input type="hidden"  id="tranId" value='<%=transId.toString()%>'>
			<input type="hidden"  id="name2" value='<%=beanDetails.get("lstName")%>'>
			<input type="hidden"  id="sem" value='<%=beanDetails.get("semester")%>'>
			
			
			
			
	<script type="text/javascript">
		function Pay() {
			<%-- alert("hello we r ready to pay");
			alert("hello map testing values:"+<%=beanDetails.get("Mobile Number")%>); --%>
			var name=document.getElementById("name1").value;
			var RollNo=document.getElementById("RollNo").value;
			var amount =document.getElementById("amount").value;
			var mobNum=document.getElementById("mobNum").value;
			var email=document.getElementById("email").value;
			var tranId=document.getElementById("tranId").value;
		
			var name2=document.getElementById("name2").value;
			var sem=document.getElementById("sem").value;
			var course=document.getElementById("course").value;
			
		 	var clientId="NITJ"; 
			
			<%-- var linkadd = "http://localhost:8080/SabPaisaCHSE/index.jsp?Name="+<%= beanDetails.get("Name") %>+"&amt="
					+
	<%=transAmount.toString()%>
		+ "&client=NITJ&Contact"+<%=beanDetails.get("Mobile Number")%>+"&Email="+<%=beanDetails.get("Email Id")%> +"&txnId=0&returnUrl=http://localhost:8080/QCollect/index.jsp";
			window.location.href = linkadd; --%>
			/* var linkadd = "http://localhost:8080/SabPaisaLive/index.jsp?firstName="+name+"&lstName="+name2+"&amt="+amount+"&Contact="+mobNum+"&Email="+email+"&txnId="+tranId+"&client="+clientId+"&returnUrl=http://localhost:8084/QCollect/returnPage.jsp"; */
			
			var linkadd = "http://localhost:8080/SabPaisaQCollect/index.jsp?firstName="+name+"&RollNo="+RollNo+"&lstName="+name2+"&amt="+amount+"&Contact="+mobNum+"&Email="+email+"&txnId="+tranId+"&client="+clientId+"&sem="+sem+"&course="+course+"&returnUrl=http://localhost:8080/QCollect/returnPage.jsp";
			window.location.href = linkadd;
		}
	</script>
</body>
</html>
