<!DOCTYPE html>
<html lang="en">
<head>

<meta charset="utf-8">
<title>SabPaisa</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="description"
	content="Charisma, a fully featured, responsive, HTML5, Bootstrap admin template.">
<meta name="author" content="Muhammad Usman">

<!-- The styles -->
<link id="bs-css" href="css/bootstrap-cerulean.min.css" rel="stylesheet">

<link href="css/charisma-app.css" rel="stylesheet">
<link href='bower_components/fullcalendar/dist/fullcalendar.css'
	rel='stylesheet'>
<link href='bower_components/fullcalendar/dist/fullcalendar.print.css'
	rel='stylesheet' media='print'>
<link href='bower_components/chosen/chosen.min.css' rel='stylesheet'>
<link href='bower_components/colorbox/example3/colorbox.css'
	rel='stylesheet'>
<link href='bower_components/responsive-tables/responsive-tables.css'
	rel='stylesheet'>
<link
	href='bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css'
	rel='stylesheet'>
<link href='css/jquery.noty.css' rel='stylesheet'>
<link href='css/noty_theme_default.css' rel='stylesheet'>
<link href='css/elfinder.min.css' rel='stylesheet'>
<link href='css/elfinder.theme.css' rel='stylesheet'>
<link href='css/jquery.iphone.toggle.css' rel='stylesheet'>
<link href='css/uploadify.css' rel='stylesheet'>
<link href='css/animate.min.css' rel='stylesheet'>
<link href="bower_components/font-awesome/css/font-awesome.min.css"
	rel="stylesheet" type="text/css">
<!-- jQuery -->
<script src="bower_components/jquery/jquery.min.js"></script>

<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

<!-- The fav icon -->
<link rel="shortcut icon" href="img/favicon.ico">



</head>

<body onload="createSession()">
	<!-- topbar starts -->


	<div class="navbar navbar-default" role="navigation">

		<div class="navbar-inner">
			<button type="button" class="navbar-toggle pull-left animated flip">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>

			<a class="navbar-brand" href="#"> <img alt="Charisma Logo"
				src="img/logo20.png" class="hidden-xs" /> <span>SabPaisa</span></a>



			<!-- theme selector starts -->
			<div class="btn-group pull-right theme-container">
				<button class="btn btn-default dropdown-toggle"
					data-toggle="dropdown">
					<i class="glyphicon glyphicon-tint"></i><span
						class="hidden-sm hidden-xs"> </span> <span class="caret"></span>
				</button>
				<ul class="dropdown-menu" id="themes">
					<li><a data-value="classic" href="#"><i class="whitespace"></i>
							Classic</a></li>
					<li><a data-value="cerulean" href="#"><i
							class="whitespace"></i> Cerulean</a></li>
					<li><a data-value="cyborg" href="#"><i class="whitespace"></i>
							Cyborg</a></li>
					<li><a data-value="simplex" href="#"><i class="whitespace"></i>
							Simplex</a></li>
					<li><a data-value="darkly" href="#"><i class="whitespace"></i>
							Darkly</a></li>
					<li><a data-value="lumen" href="#"><i class="whitespace"></i>
							Lumen</a></li>
					<li><a data-value="slate" href="#"><i class="whitespace"></i>
							Slate</a></li>
					<li><a data-value="spacelab" href="#"><i
							class="whitespace"></i> Spacelab</a></li>
					<li><a data-value="united" href="#"><i class="whitespace"></i>
							United</a></li>
				</ul>
			</div>
			<!-- theme selector ends -->

		</div>
	</div>
	<!-- topbar ends -->
	<div class="ch-container">
		<div class="row">


			<noscript>
				<div class="alert alert-block col-md-12">
					<h4 class="alert-heading">Warning!</h4>

					<p>
						You need to have <a href="http://en.wikipedia.org/wiki/JavaScript"
							target="_blank">JavaScript</a> enabled to use this site.
					</p>
				</div>
			</noscript>

			<div id="content" class="col-lg-12 col-sm-12">
				<!-- content starts -->



				<!--/row-->
				<div class="row">
					<div class="box col-md-12">
						<div class="box-inner">
							<div class="box-header well">
								<h2>
									<i class="glyphicon glyphicon-th"></i>Payment Summary
								</h2>
								<div class="box-icon">

									<a href="#" class="btn btn-minimize btn-round btn-default"><i
										class="glyphicon glyphicon-chevron-up"></i></a>

								</div>
							</div>
							<div class="box-content">
								<table class="table table-condensed">
									<tbody>
										<tr>
											<td><strong>Name</strong></td>
											<td>XYZ</td>
										</tr>
										<tr>
											<td><strong>Amount</strong></td>
											<td>5000</td>
										</tr>
										<tr>
											<td><strong>Client</strong></td>
											<td>Council of Higher Secondary Education Odisha</td>
										</tr>
									</tbody>
								</table>
							</div>
						</div>
					</div>
				</div>

				<!--/row-->

				<div class="row">
					<!-- <div class="box col-md-12">
        <div class="box-inner "> -->
					<!--  <div class="box-header well">
                <h2><i class="glyphicon glyphicon-th"></i>Choose Payment Mode</h2>
                <div class="box-icon">
                    
                    <a href="#" class="btn btn-minimize btn-round btn-default"><i
                            class="glyphicon glyphicon-chevron-up"></i></a>
                    
                </div>
            </div> -->
					<div class="box-content">



						<div class="col-md-3 col-sm-6 col-xs-6">
							<a data-toggle="tooltip"
								title="Make your payment by paying cash at a bank branch"
								class="well top-block" href="#" onclick="viewChallan()"> <i class="fa fa-inr green" ></i>

								<div>Cash</div> <%-- <script type="text/javascript">
								
								
								function callFunctionIsure()
								{
									window.open("generateIsureChallan?paymentMode=Cash&rollno=<%=request.getParameter("RollNo")%>&name=<%=request.getParameter("Name")%>&branch=<%=request.getParameter("branch")%>&sem=<%=request.getParameter("sem")%>&phone=<%=request.getParameter("Contact")%>&txnId=<%=request.getParameter("txnId")%>&amount=<%=request.getParameter("amt")%>&client=<%=request.getParameter("client")%>&ru=<%=request.getParameter("ru")%>&failureUrl=<%=request.getParameter("failureURL")%>",'',"width=1400, height=600");
								 
									}
								</script> --%>

							</a>
						</div>









						<div class="col-md-3 col-sm-3 col-xs-6">
							<a data-toggle="tooltip"
								title="Make your payment by credit or debit card using payment gateway"
								class="well top-block" href="#" onclick="viewReceipt()">
								<i class="fa fa-credit-card blue"></i>

								<div>Pay By Credit Card</div>

							</a>
						</div>


						<div class="col-md-3 col-sm-3 col-xs-6">
							<a data-toggle="tooltip"
								title="Make your payment by credit or debit card using payment gateway"
								class="well top-block" href="#"
								onclick="viewReceipt()">
								<i class="fa fa-credit-card blue"></i>

								<div>Pay By Debit Card</div>

							</a>
						</div>
					
						<div class="col-md-3 col-sm-6 col-xs-6">
							<a data-toggle="tooltip"
								title="Make your payment by logging into the internet banking page of your bank"
								class="well top-block" href="#"
								onclick="viewReceipt()">
								<i class="fa fa-bank yellow"></i>

								<div>Internet Banking</div>

							</a>
						</div>

						
						


						
						</div>

						<%-- 	<script type="text/javascript">
								
								
								function callFunctionIsureCheque()
								{
									window.open("saveChequeDetail?roll=<%=enrollmentId%>&paymentMode=Cheque&clientName=<%=client%>&payeeName=<%=payeeName%>&payeeCon=<%=payeeContact%>&payeeAdd=<%=payeeAdd%>&payeeEmail=<%=payeeEmail%>&clienTxnId=<%=clientTxnId%>&amount=<%=txnAmnt%>&clientRu=<%=clientReturnPath%>&failureUrl=<%=request.getParameter("failureURL")%>","Cheque Detail","width=550,height=600");
								}
								</script> --%>
						


				</div>

				<script type="text/javascript">
					function showCharges(linkAddress, payMode, clientName)

					{

						var ta = document.getElementById("totalAmount").value;
						var cf;
						var IHS = 10;
						var sc;
						if (payMode == 'Cash') {
							cf = 60;

							sc = (cf + IHS) * 0.14;
							sc = Math.round(sc * 100) / 100;
						} else if (payMode == 'Credit Card') {
							cf = (1.50 / 100) * ta;

							cf = Math.round(cf * 100) / 100;
							sc = (cf + IHS) * 0.14;
							sc = Math.round(sc * 100) / 100;
						}

						else if (payMode == 'Debit Card') {
							cf = (1 / 100) * ta;

							cf = Math.round(cf * 100) / 100;
							sc = (cf + IHS) * 0.14;
							sc = Math.round(sc * 100) / 100;
						} else if (payMode == 'Cheque') {
							cf = 40;

							sc = (cf + IHS) * 0.14;
							sc = Math.round(sc * 100) / 100;
						} else if (payMode == 'Net Banking') {
							cf = 25;
							sc = (cf + IHS) * 0.14;
							sc = Math.round(sc * 100) / 100;
						}

						var totalAmount = parseFloat(ta) + parseFloat(cf)
								+ parseFloat(IHS) + parseFloat(sc);
						totalAmount = Math.round(totalAmount * 100) / 100;

						document.getElementById("ihsID").innerHTML = IHS;
						document.getElementById("scID").innerHTML = sc;
						document.getElementById("cfID").innerHTML = cf;
						document.getElementById("totalAmountID").innerHTML = totalAmount;

						document.getElementById("amountwithTaxId").value = totalAmount;

						document.getElementById("ihsVal").value = IHS;
						document.getElementById("scVal").value = sc;
						document.getElementById("cfVal").value = cf;

						document.getElementById("chargesID").style.display = "block";

						document.getElementById("addressLinkId").value = linkAddress;
						document.getElementById("payMode").value = payMode;
						document.getElementById("payModeID").innerHTML = payMode;
						document.getElementById("clientNameID").innerHTML = clientName;
						document.getElementById("clientName").value = clientName;

					}
				</script>


				




				<!--/row-->

				<!--/row-->
				<!-- content ends -->
			</div>
			<!--/#content.col-md-0-->
		</div>
		<!--/fluid-row-->



		<hr>

		<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel" aria-hidden="true">

			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">�</button>
						<h3>Settings</h3>
					</div>
					<div class="modal-body">
						<p>Here settings can be configured...</p>
					</div>
					<div class="modal-footer">
						<a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
						<a href="#" class="btn btn-primary" data-dismiss="modal">Save
							changes</a>
					</div>
				</div>
			</div>
		</div>

		<!--   <footer>
        <p class="col-md-9 col-sm-9 col-xs-12 copyright">&copy; <a href="http://dexpertsystems.com" target="_blank">Dexpert Systems Pvt. Ltd</a></p>

        <p class="col-md-3 col-sm-3 col-xs-12 powered-by">Powered by: <a
                href="http://dexpertsystems.com">Dexpert</a></p>
    </footer> -->

	</div>
	<!--/.fluid-container-->

	<!-- external javascript -->

	<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

	<!-- library for cookie management -->
	<script src="js/jquery.cookie.js"></script>
	<!-- calender plugin -->
	<script src='bower_components/moment/min/moment.min.js'></script>
	<script src='bower_components/fullcalendar/dist/fullcalendar.min.js'></script>

	<!-- data table plugin -->
	<script src='js/jquery.dataTables.min.js'></script>

	<!-- select or dropdown enhancer -->
	<script src="bower_components/chosen/chosen.jquery.min.js"></script>
	<!-- plugin for gallery image view -->
	<script src="bower_components/colorbox/jquery.colorbox-min.js"></script>
	<!-- notification plugin -->
	<script src="js/jquery.noty.js"></script>
	<!-- library for making tables responsive -->
	<script src="bower_components/responsive-tables/responsive-tables.js"></script>
	<!-- tour plugin -->
	<script
		src="bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
	<!-- star rating plugin -->
	<script src="js/jquery.raty.min.js"></script>
	<!-- for iOS style toggle switch -->
	<script src="js/jquery.iphone.toggle.js"></script>
	<!-- autogrowing textarea plugin -->
	<script src="js/jquery.autogrow-textarea.js"></script>
	<!-- multiple file upload plugin -->
	<script src="js/jquery.uploadify-3.1.min.js"></script>
	<!-- history.js for cross-browser state change on ajax -->
	<script src="js/jquery.history.js"></script>
	<!-- application script for Charisma demo -->
	<script src="js/charisma.js"></script>
	<script type="text/javascript">
	function viewReceipt()
	{
		alert("Transaction was successful!");
		window.open("Receipt.jsp", "Preview","width=500,height=768");
	}
	
	function viewChallan()
	{
		alert("Transaction was successful!");
		window.open("challan.jsp", "Preview","width=500,height=768");
		
	}
	</script>

</body>
</html>
