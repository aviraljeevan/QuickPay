<!DOCTYPE html>
<html lang="en">
<%@ taglib prefix="s" uri="/struts-tags"%>
<head>

<meta charset="utf-8">
<title>QuickCollect</title>
<meta name="viewport" content="width=device-width, initial-scale=1">
<!-- The styles -->
<link id="bs-css" href="css/bootstrap-cerulean.min.css" rel="stylesheet">

<link href="css/charisma-app.css" rel="stylesheet">
<link href='bower_components/fullcalendar/dist/fullcalendar.css'
	rel='stylesheet'>
<link href='bower_components/fullcalendar/dist/fullcalendar.print.css'
	rel='stylesheet' media='print'>
<link href='bower_components/chosen/chosen.min.css' rel='stylesheet'>
<link href='bower_components/colorbox/example3/colorbox.css'
	rel='stylesheet'>
<link href='bower_components/responsive-tables/responsive-tables.css'
	rel='stylesheet'>
<link
	href='bower_components/bootstrap-tour/build/css/bootstrap-tour.min.css'
	rel='stylesheet'>
<link href='css/jquery.noty.css' rel='stylesheet'>
<link href='css/noty_theme_default.css' rel='stylesheet'>
<link href='css/elfinder.min.css' rel='stylesheet'>
<link href='css/elfinder.theme.css' rel='stylesheet'>
<link href='css/jquery.iphone.toggle.css' rel='stylesheet'>
<link href='css/uploadify.css' rel='stylesheet'>
<link href='css/animate.min.css' rel='stylesheet'>
<style type="text/css">
#tags {
	float: left;
	border: 1px solid #ccc;
	padding: 5px;
	font-family: Arial;
}

#tags span.tag {
	cursor: pointer;
	display: block;
	float: left;
	color: #fff;
	background: #689;
	padding: 5px;
	padding-right: 25px;
	margin: 4px;
}

#tags span.tag:hover {
	opacity: 0.7;
}

#tags span.tag:after {
	position: absolute;
	content: "x";
	border: 1px solid;
	padding: 0 4px;
	margin: 3px 0 10px 5px;
	font-size: 10px;
}

#tags input {
	background: #eee;
	border: 0;
	margin: 4px;
	padding: 7px;
	width: auto;
}
</style>
<!-- jQuery -->
<script src="bower_components/jquery/jquery.min.js"></script>

<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
<!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

<!-- The fav icon -->
<link rel="shortcut icon" href="img/favicon.ico">

</head>

<body>
	<!-- topbar starts -->
	<div class="navbar navbar-default" role="navigation">

		<div class="navbar-inner">
			<button type="button" class="navbar-toggle pull-left">
				<span class="sr-only">Toggle navigation</span> <span
					class="icon-bar"></span> <span class="icon-bar"></span> <span
					class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="Login.jsp"> <span>QuickCollect
					for FeeDesk</span></a>





		</div>
	</div>
	<!-- topbar ends -->
	<div class="ch-container">
		<div class="row">
			<!-- left menu starts -->
			<div class="col-sm-2 col-lg-2">
				<div class="sidebar-nav">
					<div class="nav-canvas">
						<jsp:include page="menu-Admin.jsp"></jsp:include>
					</div>
				</div>
			</div>
			<!--/span-->
			<!-- left menu ends -->

			<noscript>
				<div class="alert alert-block col-md-12">
					<h4 class="alert-heading">Warning!</h4>

					<p>
						You need to have <a href="http://en.wikipedia.org/wiki/JavaScript"
							target="_blank">JavaScript</a> enabled to use this site.
					</p>
				</div>
			</noscript>

			<div id="content" class="col-lg-10 col-sm-12">
				<!-- content starts -->




				<div class="row">
					<div class="box col-md-6">
						<div class="box-inner">
							<div class="box-header well" data-original-title="">
								<h2>
									<i class="glyphicon glyphicon-star-empty"></i> Dynamic Form
									Builder
								</h2>

								<div class="box-icon">
									<a href="#" class="btn btn-minimize btn-round btn-default"><i
										class="glyphicon glyphicon-chevron-up"></i></a>
								</div>
							</div>
							<div class="box-content">
								<!-- put your content here -->
								<form id="nitForm" action="payForm">
									<table>

										<tr>
											<td>
												<div class="control-group">
													<label class="control-label" for="selectFee">Select
														Fee</label>

													<div class="controls">
														<s:select theme="simple" list="fees" listValue="feeName"
															listKey="id" cssStyle="width:100%"
															requiredLabel="required" name="selectedFee"
															data-rel="chosen" id="selectFee" headerKey="-1"
															onchange="showFormBaseAmount()" headerValue="Select Fee">
														</s:select>
														&nbsp;
														<button type="button" onclick="showFormFee()"
															class="btn btn-sm btn-info pull-right">Add New
															Fee</button>
													</div>
												</div>
											</td>
											<td></td>

										</tr>
									</table>

									<table style="display: none" id="baseAmountDiv">

										<tr>
											<td>
												<div class="control-group">
													<label class="control-label" for="selectFee">Enter
														Base Amount for Fee</label>

													<div id="input-number" class="input-group">
														<input type="number" class="form-control"
															required="required" id="feeBaseAmount"
															placeholder="Enter Amount in Rupees"><span
															class="input-group-addon"><button type="button"
																onclick="showFormBuilder()" class="btn btn-info btn-xs">OK</button></span>
													</div>
												</div>
											</td>


										</tr>
									</table>

									<table style="display: none" id="formBuilderTable">
										<!-- <tr>
											<td><label class="control-label" for="dob">Start Date</label></td>
											<td><input name="fd.dob" required="required" type="date"
												class="form-control" id="dob"></td>
										</tr>
										<tr>
											<td><label class="control-label" for="dob">Last Date without Penalty
													</label></td>
											<td><input name="fd.dob" required="required" type="date"
												class="form-control" id="dob"></td>
										</tr>
										<tr>
											<td><label class="control-label" for="dob">Last Date with Penalty
													</label></td>
											<td><input name="fd.dob" required="required" type="date"
												class="form-control" id="dob"></td>
										</tr>
										<tr>
											<td><label class="control-label" for="dob">Late Fee (Penalty)</label></td>
											<td><input name="fd.dob" required="required" type="number"
												class="form-control" placeholder="Late Fee in Rs." id="dob"></td>
										</tr> -->
										<tr>

											<td colspan="2">
												<div class="control-group">
													<label class="control-label" for="selectFields">Select
														Form Fields</label>
													<div class="controls">
														<s:select theme="simple" list="fields"
															listValue='lookup_name +": "+ lookup_type +" ("+lookup_subtype+")"'
															listKey="lookup_id" cssStyle="width:100%"
															requiredLabel="required" name="selectedFields"
															onchange="renderPreview(this.value)" id="selectFields"
															multiple="fields"></s:select>
													</div>
												</div>
											</td>


										</tr>
										<tr>
										<td><button type="button" class="btn btn-info">Add Custom Field</button>
										</tr>

										<tr>
											<td>
												<button type="button" onclick="saveForm()"
													class="btn btn-success">Save Form</button>
											</td>


										</tr>
									</table>






								</form>
							</div>
						</div>
					</div>

					<div class="box col-md-6">
						<div class="box-inner">
							<div class="box-header well" data-original-title="">
								<h2>
									<i class="glyphicon glyphicon-star-empty"></i> Form Preview
								</h2>

								<div class="box-icon">
									<a href="#" class="btn btn-minimize btn-round btn-default"><i
										class="glyphicon glyphicon-chevron-up"></i></a>
								</div>
							</div>
							<div class="box-content" id="renderBox">
								<!-- put your content here -->

							</div>
						</div>
					</div>
				</div>
				<!--/row-->


				<!-- content ends -->
			</div>
			<!--/#content.col-md-0-->
		</div>
		<!--/fluid-row-->


		<hr>

		<div class="modal fade" id="myModal" tabindex="-1" role="dialog"
			aria-labelledby="myModalLabel" aria-hidden="true">

			<div class="modal-dialog">
				<div class="modal-content">
					<div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">�</button>
						<h3>Settings</h3>
					</div>
					<div class="modal-body">
						<p>Here settings can be configured...</p>
					</div>
					<div class="modal-footer">
						<a href="#" class="btn btn-default" data-dismiss="modal">Close</a>
						<a href="#" class="btn btn-primary" data-dismiss="modal">Save
							changes</a>
					</div>
				</div>
			</div>
		</div>
		<footer class="row">

			<div style="text-align: center;">

				<a href=""
					onclick="window.open('ContactUs.html','mywindowtitle',
											'width=500,height=550')">Contact
					Us </a> <span> |&nbsp;&nbsp;</span> <a href=""
					onclick="window.open('PrivacyPolicy.html','mywindowtitle',
											'width=500,height=550')">Privacy
					Policy </a> <span> |&nbsp;&nbsp;</span> <a href=""
					onclick="window.open('Terms And Conditions.html','mywindowtitle',
											'width=500,height=550')">Terms
					& Conditions </a> <span> |&nbsp;&nbsp;</span> <a href=""
					onclick="window.open('Disclaimer.html','mywindowtitle',
											'width=500,height=550')">Disclaimer
				</a>







			</div>


		</footer>
	</div>
	<!--/.fluid-container-->

	<!-- external javascript -->

	<script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>

	<!-- library for cookie management -->
	<script src="js/jquery.cookie.js"></script>
	<!-- calender plugin -->
	<script src='bower_components/moment/min/moment.min.js'></script>
	<script src='bower_components/fullcalendar/dist/fullcalendar.min.js'></script>
	<!-- data table plugin -->
	<script src='js/jquery.dataTables.min.js'></script>

	<!-- select or dropdown enhancer -->
	<script src="bower_components/chosen/chosen.jquery.min.js"></script>
	<!-- plugin for gallery image view -->
	<script src="bower_components/colorbox/jquery.colorbox-min.js"></script>
	<!-- notification plugin -->
	<script src="js/jquery.noty.js"></script>
	<!-- library for making tables responsive -->
	<script src="bower_components/responsive-tables/responsive-tables.js"></script>
	<!-- tour plugin -->
	<script
		src="bower_components/bootstrap-tour/build/js/bootstrap-tour.min.js"></script>
	<!-- star rating plugin -->
	<script src="js/jquery.raty.min.js"></script>
	<!-- for iOS style toggle switch -->
	<script src="js/jquery.iphone.toggle.js"></script>
	<!-- autogrowing textarea plugin -->
	<script src="js/jquery.autogrow-textarea.js"></script>
	<!-- multiple file upload plugin -->
	<script src="js/jquery.uploadify-3.1.min.js"></script>
	<!-- history.js for cross-browser state change on ajax -->
	<script src="js/jquery.history.js"></script>
	<!-- application script for Charisma demo -->
	<script src="js/charisma.js"></script>

	<script>
	var mandArray={};
	var values2 = {};
	var values = {};
	
		function GetForm(x) {
			if (x == "1") {
				document.getElementById("mainForm1").style.display = "block";
				$("#fieldSelect").chosen();
			}

		}		
		
		function Reset() {
			document.getElementById("nitForm").reset();
			document.getElementById("customTags").innerHTML = " ";
		}
		
		function showFormFee() {
			window.open("formFee.jsp", "Preview", "width=640,height=480");
		}

		function showFormBaseAmount() {
			document.getElementById("baseAmountDiv").style.display = "block";
		}

		
		function AddToArray(value, name, id) {
			value = value.split(",").join("");
			value = value.split("`").join("");
			value = value.split("=").join("");
			values[id] = id + "`" + name + "=" + value;
			/* alert(JSON.stringify(values)); */

		}

		function renderPreview(x) {
			var select1 = document.getElementById("selectFields");
			var selected1 = new Array;
			for (var i = 0; i < select1.length; i++) {
				if (select1.options[i].selected)
					selected1.push(select1.options[i].value);
			}
			var xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function() {
				if (xhttp.readyState == 4 && xhttp.status == 200) {
					document.getElementById("renderBox").innerHTML = xhttp.responseText;
				}
			}
			xhttp.open("GET", "renderPreview?fieldarray=" + selected1
					+ "&feeAmount=" + baseAmount, true);
			xhttp.send();

		}
		var baseAmount = "";
		function showFormBuilder() {
			baseAmount = document.getElementById("feeBaseAmount").value;
			if (baseAmount == "") {
				alert("Please Enter the Base Amount for the Fee");
				return;
			} else {
				document.getElementById("formBuilderTable").style.display = "block";
				$("#selectFields").chosen();
			}
		}
	
		
		function AddToArray2() {
			var value=document.getElementById("optionInput").value;
			var amount=document.getElementById("optionAmount").value;
			if(amount=="")
				{
				amount="0";
				}
			 value = value.split(",").join("");
				amount = amount.split(",").join("");
				values2[value] = value+"="+amount;
			  /* alert(JSON.stringify(values2));  */
			 var presentTags=document.getElementById("tagDiv").innerHTML;
			 document.getElementById("tagDiv").innerHTML=presentTags+'<br> <span id="'+value+'">' + value +' for Rs.'+amount+'  <a title="Remove This Option" href="#"><i id="'+value+'" onclick="RemoveFromValues2(this.id)" class="glyphicon glyphicon-remove"> </i></a></span>';
			
			 document.getElementById("optionInput").value="";
			 document.getElementById("optionAmount").value="";

		}

		function RemoveFromValues2(value) {
			delete values2[value];
			var parent = document.getElementById("tagDiv");
			var child = document.getElementById(value);
			parent.removeChild(child);
			
		}

		function saveValues(id) {
			var id=document.getElementById("optionId").value;
			var dataArray = new Array;
			for ( var value in values2) {
				dataArray.push(values2[value]);
			}
			if(dataArray=="")
				{
				alert("Please Enter Atleast One Option");
				return;
				}
			var xhttp = new XMLHttpRequest();
			xhttp.onreadystatechange = function() {
				if (xhttp.readyState == 4 && xhttp.status == 200) {
					document.getElementById("renderBox").innerHTML = xhttp.responseText;
				} 
			}
			xhttp.open("GET", "saveOptions?optionArray=" + dataArray
					+ "&fieldId=" + id, true);
			xhttp.send();
			renderPreview(1);
			values2={};
		}
		
		function AddToMandatoryArray(x)
		{
			var box=document.getElementById(x);
			if(box.checked)
				{
				mandArray[x]=x;
				}
			else
				{
				delete mandArray[x];
				}
			/* alert(JSON.stringify(mandArray)); */
		}
	</script>
</body>
</html>
