<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib prefix="s" uri="/struts-tags"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>NITJ</title>
</head>
<body onload="waitSubmit()">
	<h2>Please Wait</h2>
	<%-- <%
		String msg = request.getParameter("RPS");
		String transId = request.getParameter("txnId");
	%> --%>
	<%-- <%
		if (msg.equals("0")) {
	%> --%>

	<div>While Processing...</div>
	
		<div style="text-align: center;" id="wait"><img src="img/ajax-loaders/ajax-loader-6.gif"
                                 title="img/ajax-loaders/ajax-loader-6.gif"></div>
	<%-- 	<%
		}

		else {
	%> --%>
	<!-- <div>Sorry, your transaction has been declined due to some
		reason,please wait while you are being redirected to your page...</div> -->
	<%-- 
	<%
		}
	%>
 --%>
	<script type="text/javascript">
		function waitSubmit() {

			setTimeout(function() {
				window.location = "ResponseAction";
			}, 300);

		}
	</script>

</body>
</html>